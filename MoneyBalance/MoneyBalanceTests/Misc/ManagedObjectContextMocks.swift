//
//  ManagedObjectContextMocks.swift
//  MoneyBalanceTests
//
//  Created by Arnold Leitli on 2022. 03. 06..
//

import CoreData
@testable import MoneyBalance

class NSManagedObjectContextProtocolMock: NSManagedObjectContextProtocol {
    var hasChanges: Bool {
        get { return underlyingHasChanges }
        set(value) { underlyingHasChanges = value }
    }
    var underlyingHasChanges: Bool!

    //MARK: - fetch<T>

    var fetchRequestThrowableError: Error?
    var fetchRequestCallsCount = 0
    var fetchRequestCalled: Bool {
        return fetchRequestCallsCount > 0
    }
    var fetchRequestReceivedRequest: AnyObject?
    var fetchRequestReceivedInvocations: [AnyObject] = []
    var fetchRequestReturnValue: [AnyObject]!
    var fetchRequestClosure: ((AnyObject) -> AnyObject)?

    func fetch<T>(_ request: NSFetchRequest<T>) throws -> [T] where T : NSFetchRequestResult {
        if let error = fetchRequestThrowableError {
            throw error
        }
        fetchRequestCallsCount += 1
        fetchRequestReceivedRequest = request
        fetchRequestReceivedInvocations.append(request)
        return fetchRequestClosure.map({ $0(request) as! [T] }) ?? fetchRequestReturnValue as! [T]
    }

    //MARK: - save

    var saveThrowableError: Error?
    var saveCallsCount = 0
    var saveCalled: Bool {
        return saveCallsCount > 0
    }
    var saveClosure: (() throws -> Void)?

    func save() throws {
        if let error = saveThrowableError {
            throw error
        }
        saveCallsCount += 1
        try saveClosure?()
    }

    //MARK: - obtainPermanentIDs

    var obtainPermanentIDsForThrowableError: Error?
    var obtainPermanentIDsForCallsCount = 0
    var obtainPermanentIDsForCalled: Bool {
        return obtainPermanentIDsForCallsCount > 0
    }
    var obtainPermanentIDsForReceivedObjects: [NSManagedObject]?
    var obtainPermanentIDsForReceivedInvocations: [[NSManagedObject]] = []
    var obtainPermanentIDsForClosure: (([NSManagedObject]) throws -> Void)?

    func obtainPermanentIDs(for objects: [NSManagedObject]) throws {
        if let error = obtainPermanentIDsForThrowableError {
            throw error
        }
        obtainPermanentIDsForCallsCount += 1
        obtainPermanentIDsForReceivedObjects = objects
        obtainPermanentIDsForReceivedInvocations.append(objects)
        try obtainPermanentIDsForClosure?(objects)
    }

    //MARK: - existingObject

    var existingObjectWithThrowableError: Error?
    var existingObjectWithCallsCount = 0
    var existingObjectWithCalled: Bool {
        return existingObjectWithCallsCount > 0
    }
    var existingObjectWithReceivedObjectID: NSManagedObjectID?
    var existingObjectWithReceivedInvocations: [NSManagedObjectID] = []
    var existingObjectWithReturnValue: NSManagedObject!
    var existingObjectWithClosure: ((NSManagedObjectID) throws -> NSManagedObject)?

    func existingObject(with objectID: NSManagedObjectID) throws -> NSManagedObject {
        if let error = existingObjectWithThrowableError {
            throw error
        }
        existingObjectWithCallsCount += 1
        existingObjectWithReceivedObjectID = objectID
        existingObjectWithReceivedInvocations.append(objectID)
        return try existingObjectWithClosure.map({ try $0(objectID) }) ?? existingObjectWithReturnValue
    }

    //MARK: - delete

    var deleteObjectCallsCount = 0
    var deleteObjectCalled: Bool {
        return deleteObjectCallsCount > 0
    }
    var deleteObjectReceivedObject: NSManagedObject?
    var deleteObjectReceivedInvocations: [NSManagedObject] = []
    var deleteObjectClosure: ((NSManagedObject) -> Void)?

    func delete(_ object: NSManagedObject) {
        deleteObjectCallsCount += 1
        deleteObjectReceivedObject = object
        deleteObjectReceivedInvocations.append(object)
        deleteObjectClosure?(object)
    }

}
