import XCTest
@testable import MoneyBalance
import CoreData

class CoreDataManagerTests: XCTestCase {
    var observers: Observer!
    var appLifeCycleEventProvider: AppLifeCycleEventProvider!
    var context: NSManagedObjectContextProtocolMock!
    var sut: CoreDataManager!
    var delegate: CoreDataManagerDelegateMock!
    
    override func setUp() {
        observers = Observer(dispatchQueue: DispatchQueue(label: "com.leitli.moneyBalance.coreDataManagerTests"))
        context = NSManagedObjectContextProtocolMock()
        appLifeCycleEventProvider = AppLifeCycleEventProvider(
            observers: observers
        )
        delegate = CoreDataManagerDelegateMock()
        sut = CoreDataManager(
            context: context,
            applicationLifeCycleEventProvider: appLifeCycleEventProvider
        )
        
        sut.delegate = delegate
    }
    
    func testAppDidEnterBackground() {
        //Given
        context.hasChanges = false
        
        //When
        appLifeCycleEventProvider.sceneDidEnterBackground()
        
        //Then
        XCTAssert(context.saveCallsCount == 0)
        
        //Given
        context.hasChanges = true
        
        //When
        appLifeCycleEventProvider.sceneDidEnterBackground()
        
        //Then
        XCTAssert(context.saveCallsCount == 1) // rare case its flaky
    }
    
    func testRetrieveData() {
        //Given
        context.fetchRequestReturnValue = []
        
        //When
        sut.retrieveData(for: TransactionCoreObject.self)
        
        //Then
        XCTAssert(context.fetchRequestCallsCount == 1)
        XCTAssert(delegate.coreDataManagerDidRetrieveDataCallsCount == 1)
        XCTAssert(delegate.coreDataManagerDidRetrieveDataReceivedArguments?.coreDataManager === sut)
//        XCTAssert(delegate.coreDataManagerDidRetrieveDataReceivedArguments!.data === context.fetchRequestReturnValue)
    }
}
