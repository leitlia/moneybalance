//
//  TestTransactionCoreObject.swift
//  MoneyBalanceTests
//
//  Created by Arnold Leitli on 2022. 03. 06..
//

import CoreData
@testable import MoneyBalance

class TestTransactionCoreObject {
    private(set) var storeContainer: NSPersistentContainer
    var context: NSManagedObjectContext {
        storeContainer.viewContext
    }
    
    init() {
        let persistentStoreDescription = NSPersistentStoreDescription()
        persistentStoreDescription.type = NSInMemoryStoreType
        
        let container = NSPersistentContainer(name: "MoneyBalance")
        container.persistentStoreDescriptions = [persistentStoreDescription]

        container.loadPersistentStores { _, error in
            if let error = error as NSError? {
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        }
        
        storeContainer = container
    }
    
    func saveTransactions(_ transactions: [Transaction]) -> [TransactionCoreObject] {
        var result = [TransactionCoreObject]()
        for transaction in transactions {
            let newItem = TransactionCoreObject(context: context)
            
            newItem.amount = Int64(transaction.amount.integerValue)
            newItem.transactionDescription = transaction.transactionDescription
            newItem.createdAt = transaction.createdAt
            newItem.id = transaction.uuid
            newItem.transactionType = Int16(transaction.type.rawValue)
            
            do {
                try context.save()
                try context.obtainPermanentIDs(for: [newItem])
                var newTransaction = transaction
                newTransaction.persistReference = newItem.objectID
                result.append(newItem)
            } catch {}
        }
        
        return result
    }
    
    
}
