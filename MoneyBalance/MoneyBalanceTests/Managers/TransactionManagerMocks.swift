//
//  TransactionManagerDelegateMock.swift
//  MoneyBalance
//
//  Created by Arnold Leitli on 2022. 03. 06..
//

import Foundation
@testable import MoneyBalance

class TransactionManagerDelegateMock: TransactionManagerDelegate {

    //MARK: - transactionManager

    var transactionManagerDidRetrieveTransactionsCallsCount = 0
    var transactionManagerDidRetrieveTransactionsCalled: Bool {
        return transactionManagerDidRetrieveTransactionsCallsCount > 0
    }
    var transactionManagerDidRetrieveTransactionsReceivedArguments: (transactionManager: TransactionManagerProtocol, transactions: [Transaction])?
    var transactionManagerDidRetrieveTransactionsReceivedInvocations: [(transactionManager: TransactionManagerProtocol, transactions: [Transaction])] = []
    var transactionManagerDidRetrieveTransactionsClosure: ((TransactionManagerProtocol, [Transaction]) -> Void)?

    func transactionManager(_ transactionManager: TransactionManagerProtocol, didRetrieveTransactions transactions: [Transaction]) {
        transactionManagerDidRetrieveTransactionsCallsCount += 1
        transactionManagerDidRetrieveTransactionsReceivedArguments = (transactionManager: transactionManager, transactions: transactions)
        transactionManagerDidRetrieveTransactionsReceivedInvocations.append((transactionManager: transactionManager, transactions: transactions))
        transactionManagerDidRetrieveTransactionsClosure?(transactionManager, transactions)
    }

    //MARK: - transactionManager

    var transactionManagerDidAddTransactionInCallsCount = 0
    var transactionManagerDidAddTransactionInCalled: Bool {
        return transactionManagerDidAddTransactionInCallsCount > 0
    }
    var transactionManagerDidAddTransactionInReceivedArguments: (transactionManager: TransactionManagerProtocol, transaction: Transaction, transactions: [Transaction])?
    var transactionManagerDidAddTransactionInReceivedInvocations: [(transactionManager: TransactionManagerProtocol, transaction: Transaction, transactions: [Transaction])] = []
    var transactionManagerDidAddTransactionInClosure: ((TransactionManagerProtocol, Transaction, [Transaction]) -> Void)?

    func transactionManager(_ transactionManager: TransactionManagerProtocol, didAddTransaction transaction: Transaction, in transactions: [Transaction]) {
        transactionManagerDidAddTransactionInCallsCount += 1
        transactionManagerDidAddTransactionInReceivedArguments = (transactionManager: transactionManager, transaction: transaction, transactions: transactions)
        transactionManagerDidAddTransactionInReceivedInvocations.append((transactionManager: transactionManager, transaction: transaction, transactions: transactions))
        transactionManagerDidAddTransactionInClosure?(transactionManager, transaction, transactions)
    }

    //MARK: - transactionManager

    var transactionManagerDidRemoveTransactionFromCallsCount = 0
    var transactionManagerDidRemoveTransactionFromCalled: Bool {
        return transactionManagerDidRemoveTransactionFromCallsCount > 0
    }
    var transactionManagerDidRemoveTransactionFromReceivedArguments: (transactionManager: TransactionManagerProtocol, transaction: Transaction, transactions: [Transaction])?
    var transactionManagerDidRemoveTransactionFromReceivedInvocations: [(transactionManager: TransactionManagerProtocol, transaction: Transaction, transactions: [Transaction])] = []
    var transactionManagerDidRemoveTransactionFromClosure: ((TransactionManagerProtocol, Transaction, [Transaction]) -> Void)?

    func transactionManager(_ transactionManager: TransactionManagerProtocol, didRemoveTransaction transaction: Transaction, from transactions: [Transaction]) {
        transactionManagerDidRemoveTransactionFromCallsCount += 1
        transactionManagerDidRemoveTransactionFromReceivedArguments = (transactionManager: transactionManager, transaction: transaction, transactions: transactions)
        transactionManagerDidRemoveTransactionFromReceivedInvocations.append((transactionManager: transactionManager, transaction: transaction, transactions: transactions))
        transactionManagerDidRemoveTransactionFromClosure?(transactionManager, transaction, transactions)
    }

    //MARK: - transactionManager

    var transactionManagerDidFailToRetrieveTransactionsCallsCount = 0
    var transactionManagerDidFailToRetrieveTransactionsCalled: Bool {
        return transactionManagerDidFailToRetrieveTransactionsCallsCount > 0
    }
    var transactionManagerDidFailToRetrieveTransactionsReceivedArguments: (transactionManager: TransactionManagerProtocol, error: TransactionError)?
    var transactionManagerDidFailToRetrieveTransactionsReceivedInvocations: [(transactionManager: TransactionManagerProtocol, error: TransactionError)] = []
    var transactionManagerDidFailToRetrieveTransactionsClosure: ((TransactionManagerProtocol, TransactionError) -> Void)?

    func transactionManager(_ transactionManager: TransactionManagerProtocol, didFailToRetrieveTransactions error: TransactionError) {
        transactionManagerDidFailToRetrieveTransactionsCallsCount += 1
        transactionManagerDidFailToRetrieveTransactionsReceivedArguments = (transactionManager: transactionManager, error: error)
        transactionManagerDidFailToRetrieveTransactionsReceivedInvocations.append((transactionManager: transactionManager, error: error))
        transactionManagerDidFailToRetrieveTransactionsClosure?(transactionManager, error)
    }

    //MARK: - transactionManager

    var transactionManagerDidFailToAppendTransactionErrorCallsCount = 0
    var transactionManagerDidFailToAppendTransactionErrorCalled: Bool {
        return transactionManagerDidFailToAppendTransactionErrorCallsCount > 0
    }
    var transactionManagerDidFailToAppendTransactionErrorReceivedArguments: (transactionManager: TransactionManagerProtocol, transaction: Transaction, error: TransactionError)?
    var transactionManagerDidFailToAppendTransactionErrorReceivedInvocations: [(transactionManager: TransactionManagerProtocol, transaction: Transaction, error: TransactionError)] = []
    var transactionManagerDidFailToAppendTransactionErrorClosure: ((TransactionManagerProtocol, Transaction, TransactionError) -> Void)?

    func transactionManager(_ transactionManager: TransactionManagerProtocol, didFailToAppendTransaction transaction: Transaction, error: TransactionError) {
        transactionManagerDidFailToAppendTransactionErrorCallsCount += 1
        transactionManagerDidFailToAppendTransactionErrorReceivedArguments = (transactionManager: transactionManager, transaction: transaction, error: error)
        transactionManagerDidFailToAppendTransactionErrorReceivedInvocations.append((transactionManager: transactionManager, transaction: transaction, error: error))
        transactionManagerDidFailToAppendTransactionErrorClosure?(transactionManager, transaction, error)
    }

    //MARK: - transactionManager

    var transactionManagerDidFailToRemoveTransactionErrorCallsCount = 0
    var transactionManagerDidFailToRemoveTransactionErrorCalled: Bool {
        return transactionManagerDidFailToRemoveTransactionErrorCallsCount > 0
    }
    var transactionManagerDidFailToRemoveTransactionErrorReceivedArguments: (transactionManager: TransactionManagerProtocol, transaction: Transaction, error: TransactionError)?
    var transactionManagerDidFailToRemoveTransactionErrorReceivedInvocations: [(transactionManager: TransactionManagerProtocol, transaction: Transaction, error: TransactionError)] = []
    var transactionManagerDidFailToRemoveTransactionErrorClosure: ((TransactionManagerProtocol, Transaction, TransactionError) -> Void)?

    func transactionManager(_ transactionManager: TransactionManagerProtocol, didFailToRemoveTransaction transaction: Transaction, error: TransactionError) {
        transactionManagerDidFailToRemoveTransactionErrorCallsCount += 1
        transactionManagerDidFailToRemoveTransactionErrorReceivedArguments = (transactionManager: transactionManager, transaction: transaction, error: error)
        transactionManagerDidFailToRemoveTransactionErrorReceivedInvocations.append((transactionManager: transactionManager, transaction: transaction, error: error))
        transactionManagerDidFailToRemoveTransactionErrorClosure?(transactionManager, transaction, error)
    }

}
