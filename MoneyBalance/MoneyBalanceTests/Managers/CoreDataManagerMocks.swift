import CoreData
@testable import MoneyBalance

class CoreDataManagerTransactionProtocolMock: CoreDataManagerTransactionProtocol {
    var delegate: CoreDataManagerDelegate?

    //MARK: - save

    var saveTransactionCallsCount = 0
    var saveTransactionCalled: Bool {
        return saveTransactionCallsCount > 0
    }
    var saveTransactionReceivedTransaction: Transaction?
    var saveTransactionReceivedInvocations: [Transaction] = []
    var saveTransactionClosure: ((Transaction) -> Void)?

    func save(transaction: Transaction) {
        saveTransactionCallsCount += 1
        saveTransactionReceivedTransaction = transaction
        saveTransactionReceivedInvocations.append(transaction)
        saveTransactionClosure?(transaction)
    }

    //MARK: - remove

    var removeTransactionCallsCount = 0
    var removeTransactionCalled: Bool {
        return removeTransactionCallsCount > 0
    }
    var removeTransactionReceivedTransaction: Transaction?
    var removeTransactionReceivedInvocations: [Transaction] = []
    var removeTransactionClosure: ((Transaction) -> Void)?

    func remove(transaction: Transaction) {
        removeTransactionCallsCount += 1
        removeTransactionReceivedTransaction = transaction
        removeTransactionReceivedInvocations.append(transaction)
        removeTransactionClosure?(transaction)
    }

    //MARK: - retrieveData

    var retrieveDataForCallsCount = 0
    var retrieveDataForCalled: Bool {
        return retrieveDataForCallsCount > 0
    }
    var retrieveDataForReceivedObject: NSManagedObject.Type?
    var retrieveDataForReceivedInvocations: [NSManagedObject.Type] = []
    var retrieveDataForClosure: ((NSManagedObject.Type) -> Void)?

    func retrieveData(for object: NSManagedObject.Type) {
        retrieveDataForCallsCount += 1
        retrieveDataForReceivedObject = object
        retrieveDataForReceivedInvocations.append(object)
        retrieveDataForClosure?(object)
    }

}


class CoreDataManagerDelegateMock: CoreDataManagerDelegate {

    //MARK: - coreDataManager

    var coreDataManagerDidRetrieveDataCallsCount = 0
    var coreDataManagerDidRetrieveDataCalled: Bool {
        return coreDataManagerDidRetrieveDataCallsCount > 0
    }
    var coreDataManagerDidRetrieveDataReceivedArguments: (coreDataManager: CoreDataManagerProtocol, data: [Any])?
    var coreDataManagerDidRetrieveDataReceivedInvocations: [(coreDataManager: CoreDataManagerProtocol, data: [Any])] = []
    var coreDataManagerDidRetrieveDataClosure: ((CoreDataManagerProtocol, [Any]) -> Void)?

    func coreDataManager(_ coreDataManager: CoreDataManagerProtocol, didRetrieveData data: [Any]) {
        coreDataManagerDidRetrieveDataCallsCount += 1
        coreDataManagerDidRetrieveDataReceivedArguments = (coreDataManager: coreDataManager, data: data)
        coreDataManagerDidRetrieveDataReceivedInvocations.append((coreDataManager: coreDataManager, data: data))
        coreDataManagerDidRetrieveDataClosure?(coreDataManager, data)
    }

    //MARK: - coreDataManager

    var coreDataManagerDidSaveDataCallsCount = 0
    var coreDataManagerDidSaveDataCalled: Bool {
        return coreDataManagerDidSaveDataCallsCount > 0
    }
    var coreDataManagerDidSaveDataReceivedArguments: (coreDataManager: CoreDataManagerProtocol, data: Any)?
    var coreDataManagerDidSaveDataReceivedInvocations: [(coreDataManager: CoreDataManagerProtocol, data: Any)] = []
    var coreDataManagerDidSaveDataClosure: ((CoreDataManagerProtocol, Any) -> Void)?

    func coreDataManager(_ coreDataManager: CoreDataManagerProtocol, didSaveData data: Any) {
        coreDataManagerDidSaveDataCallsCount += 1
        coreDataManagerDidSaveDataReceivedArguments = (coreDataManager: coreDataManager, data: data)
        coreDataManagerDidSaveDataReceivedInvocations.append((coreDataManager: coreDataManager, data: data))
        coreDataManagerDidSaveDataClosure?(coreDataManager, data)
    }

    //MARK: - coreDataManager

    var coreDataManagerDidFailSaveDataCallsCount = 0
    var coreDataManagerDidFailSaveDataCalled: Bool {
        return coreDataManagerDidFailSaveDataCallsCount > 0
    }
    var coreDataManagerDidFailSaveDataReceivedArguments: (coreDataManager: CoreDataManagerProtocol, data: Any)?
    var coreDataManagerDidFailSaveDataReceivedInvocations: [(coreDataManager: CoreDataManagerProtocol, data: Any)] = []
    var coreDataManagerDidFailSaveDataClosure: ((CoreDataManagerProtocol, Any) -> Void)?

    func coreDataManager(_ coreDataManager: CoreDataManagerProtocol, didFailSaveData data: Any) {
        coreDataManagerDidFailSaveDataCallsCount += 1
        coreDataManagerDidFailSaveDataReceivedArguments = (coreDataManager: coreDataManager, data: data)
        coreDataManagerDidFailSaveDataReceivedInvocations.append((coreDataManager: coreDataManager, data: data))
        coreDataManagerDidFailSaveDataClosure?(coreDataManager, data)
    }

    //MARK: - coreDataManager

    var coreDataManagerDidRemoveDataCallsCount = 0
    var coreDataManagerDidRemoveDataCalled: Bool {
        return coreDataManagerDidRemoveDataCallsCount > 0
    }
    var coreDataManagerDidRemoveDataReceivedArguments: (coreDataManager: CoreDataManagerProtocol, data: Any)?
    var coreDataManagerDidRemoveDataReceivedInvocations: [(coreDataManager: CoreDataManagerProtocol, data: Any)] = []
    var coreDataManagerDidRemoveDataClosure: ((CoreDataManagerProtocol, Any) -> Void)?

    func coreDataManager(_ coreDataManager: CoreDataManagerProtocol, didRemoveData data: Any) {
        coreDataManagerDidRemoveDataCallsCount += 1
        coreDataManagerDidRemoveDataReceivedArguments = (coreDataManager: coreDataManager, data: data)
        coreDataManagerDidRemoveDataReceivedInvocations.append((coreDataManager: coreDataManager, data: data))
        coreDataManagerDidRemoveDataClosure?(coreDataManager, data)
    }

    //MARK: - coreDataManager

    var coreDataManagerDidFailRemoveDataCallsCount = 0
    var coreDataManagerDidFailRemoveDataCalled: Bool {
        return coreDataManagerDidFailRemoveDataCallsCount > 0
    }
    var coreDataManagerDidFailRemoveDataReceivedArguments: (coreDataManager: CoreDataManagerProtocol, data: Any)?
    var coreDataManagerDidFailRemoveDataReceivedInvocations: [(coreDataManager: CoreDataManagerProtocol, data: Any)] = []
    var coreDataManagerDidFailRemoveDataClosure: ((CoreDataManagerProtocol, Any) -> Void)?

    func coreDataManager(_ coreDataManager: CoreDataManagerProtocol, didFailRemoveData data: Any) {
        coreDataManagerDidFailRemoveDataCallsCount += 1
        coreDataManagerDidFailRemoveDataReceivedArguments = (coreDataManager: coreDataManager, data: data)
        coreDataManagerDidFailRemoveDataReceivedInvocations.append((coreDataManager: coreDataManager, data: data))
        coreDataManagerDidFailRemoveDataClosure?(coreDataManager, data)
    }

}

