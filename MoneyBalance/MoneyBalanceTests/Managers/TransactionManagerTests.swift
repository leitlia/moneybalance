//
//  TransactionManagerTests.swift
//  MoneyBalanceTests
//
//  Created by Arnold Leitli on 2022. 03. 06..
//

@testable import MoneyBalance
import XCTest

class TransactionManagerTests: XCTestCase {
    var observers: Observer!
    var coreDataManagerMock: CoreDataManagerTransactionProtocolMock!
    var sut: TransactionManager!
    var transactionDelegateObserver: TransactionManagerDelegateMock!
    var testTransactionCoreObject: TestTransactionCoreObject!

    override func setUp() {
        super.setUp()
        observers = Observer(
            dispatchQueue: DispatchQueue(label: "com.leitli.moneyBalance.transactionManagerTests")
        )
        
        coreDataManagerMock = CoreDataManagerTransactionProtocolMock()
        
        sut = TransactionManager(
            coreDataManager: coreDataManagerMock,
            observers: observers
        )

        transactionDelegateObserver = TransactionManagerDelegateMock()
        sut.addObserver(transactionDelegateObserver)
        testTransactionCoreObject = TestTransactionCoreObject()
    }
    
    func testAppendTransaction() {
//        coreDataManager.save(transaction: transaction)
        // Given
        let savingTransaction = Transaction()
        coreDataManagerMock.saveTransactionClosure = { transaction in
            self.coreDataManagerMock.delegate?.coreDataManager(self.coreDataManagerMock, didSaveData: transaction)
        }
        
        // When
        sut.appendTransaction(savingTransaction)
        
        // Then
        XCTAssert(coreDataManagerMock.saveTransactionCallsCount == 1)
        XCTAssert(coreDataManagerMock.saveTransactionReceivedTransaction == savingTransaction)
        XCTAssert(transactionDelegateObserver.transactionManagerDidAddTransactionInCallsCount == 1)
        XCTAssert(transactionDelegateObserver.transactionManagerDidAddTransactionInReceivedArguments?.transaction == savingTransaction)
        XCTAssert(transactionDelegateObserver.transactionManagerDidAddTransactionInReceivedArguments?.transactions.count == 1)
        
        // Given
        let savingTransaction2 = Transaction()
        coreDataManagerMock.saveTransactionClosure = { transaction in
            self.coreDataManagerMock.delegate?.coreDataManager(self.coreDataManagerMock, didFailSaveData: transaction)
        }
        
        // When
        sut.appendTransaction(savingTransaction2)
        
        // Then
        XCTAssert(coreDataManagerMock.saveTransactionCallsCount == 2)
        XCTAssert(coreDataManagerMock.saveTransactionReceivedTransaction == savingTransaction2)
        XCTAssert(transactionDelegateObserver.transactionManagerDidAddTransactionInCallsCount == 1)
        XCTAssert(transactionDelegateObserver.transactionManagerDidFailToAppendTransactionErrorCallsCount == 1)
        XCTAssert(transactionDelegateObserver.transactionManagerDidFailToAppendTransactionErrorReceivedArguments?.transaction == savingTransaction2)
        XCTAssert(transactionDelegateObserver.transactionManagerDidFailToAppendTransactionErrorReceivedArguments?.error == .coreData)
    }

    func testRemoveTransaction() {
        // Given
        let transactions = [
            Transaction(amount: Amount(Int.random(in: 1..<400)), transactionDescription: "1 item"),
            Transaction(amount: Amount(Int.random(in: 1..<400)), transactionDescription: "2 item"),
            Transaction(amount: Amount(Int.random(in: 1..<400)), transactionDescription: "3 item"),
            Transaction(amount: Amount(Int.random(in: 1..<400)), transactionDescription: "4 item"),
            Transaction(amount: Amount(Int.random(in: 1..<400)), transactionDescription: "5 item")
        ]
        let convertedManagedObjects = testTransactionCoreObject.saveTransactions(transactions)
        
        coreDataManagerMock.retrieveDataForClosure = { Type in
            self.coreDataManagerMock.delegate?.coreDataManager(self.coreDataManagerMock, didRetrieveData: convertedManagedObjects)
        }
        
        sut.retrieveTransactions()
        
        let retrievedTransactions = transactionDelegateObserver.transactionManagerDidRetrieveTransactionsReceivedArguments!.transactions
        
        let transactionToRemove = retrievedTransactions[0]
        
        coreDataManagerMock.removeTransactionClosure = { transaction in
            self.coreDataManagerMock.delegate?.coreDataManager(self.coreDataManagerMock, didRemoveData: transactionToRemove)
        }
        // When
        
        sut.removeTransaction(transactionToRemove)
        // Then
        
        XCTAssert(transactionDelegateObserver.transactionManagerDidRemoveTransactionFromCallsCount == 1)
        XCTAssert(transactionDelegateObserver.transactionManagerDidRemoveTransactionFromReceivedArguments?.transaction == transactionToRemove)
        XCTAssert(transactionDelegateObserver.transactionManagerDidRemoveTransactionFromReceivedArguments?.transactions.count == 4)
        
        let newTransactions = transactionDelegateObserver.transactionManagerDidRemoveTransactionFromReceivedArguments!.transactions
        
        XCTAssertFalse(newTransactions.contains(transactionToRemove))
        
        //Given
        let transactionToRemove2 = newTransactions[0]
        
        coreDataManagerMock.removeTransactionClosure = { transaction in
            self.coreDataManagerMock.delegate?.coreDataManager(self.coreDataManagerMock, didFailRemoveData: transactionToRemove2)
        }
        // When
        
        sut.removeTransaction(transactionToRemove2)
        // Then
        
        XCTAssert(transactionDelegateObserver.transactionManagerDidFailToRemoveTransactionErrorCallsCount == 1)
        XCTAssert(transactionDelegateObserver.transactionManagerDidFailToRemoveTransactionErrorReceivedArguments?.transaction == transactionToRemove2)
        XCTAssert(transactionDelegateObserver.transactionManagerDidFailToRemoveTransactionErrorReceivedArguments?.error == .coreData)
    }

    func testRetrieveTransactions() {
//        coreDataManager.retrieveData(for: TransactionCoreObject.self)
        // Given
        let transactions = [
            Transaction(amount: Amount(Int.random(in: 1..<400)), transactionDescription: "1 item"),
            Transaction(amount: Amount(Int.random(in: 1..<400)), transactionDescription: "2 item"),
            Transaction(amount: Amount(Int.random(in: 1..<400)), transactionDescription: "3 item"),
            Transaction(amount: Amount(Int.random(in: 1..<400)), transactionDescription: "4 item"),
            Transaction(amount: Amount(Int.random(in: 1..<400)), transactionDescription: "5 item")
        ]
        let convertedManagedObjects = testTransactionCoreObject.saveTransactions(transactions)
        
        coreDataManagerMock.retrieveDataForClosure = { Type in
            self.coreDataManagerMock.delegate?.coreDataManager(self.coreDataManagerMock, didRetrieveData: convertedManagedObjects)
        }
        // When
        sut.retrieveTransactions()
        
        // Then
        XCTAssert(transactionDelegateObserver.transactionManagerDidRetrieveTransactionsCallsCount == 1)
        XCTAssert(transactionDelegateObserver.transactionManagerDidRetrieveTransactionsReceivedArguments?.transactions.count == transactions.count)
        
        for transaction in transactions {
            XCTAssert(transactionDelegateObserver.transactionManagerDidRetrieveTransactionsReceivedArguments!.transactions.contains(transaction))
        }
    }
}
