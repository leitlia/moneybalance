//
//  TransactionListModuleTests.swift
//  MoneyBalanceTests
//
//  Created by Arnold Leitli on 2022. 03. 06..
//

import XCTest
@testable import MoneyBalance

class TransactionListModuleTests: XCTestCase {
    var wireframe: TransactionListWireframeProtocolMock!
    var interactor: TransactionListInteractor!
    var presenter: TransactionListPresenter!
    var view: TransactionListViewProtocolMock!
    
    var observers: Observer!
    var coreDataManagerMock: CoreDataManagerTransactionProtocolMock!
    var transactionManager: TransactionManager!
    var testTransactionCoreObject: TestTransactionCoreObject!
    
    override func setUp() {
        coreDataManagerMock = CoreDataManagerTransactionProtocolMock()
        observers = Observer(
            dispatchQueue: DispatchQueue(label: "com.leitli.moneyBalance.transactionListModuleTests")
        )
        
        transactionManager = TransactionManager(
            coreDataManager: coreDataManagerMock,
            observers: observers
        )
        wireframe = TransactionListWireframeProtocolMock()
        interactor = TransactionListInteractor(transactionManager: transactionManager)
        presenter = TransactionListPresenter()
        interactor.presenter = presenter
        presenter.interactor = interactor
        presenter.wireframe = wireframe

        view = TransactionListViewProtocolMock()
        presenter.view = view
        view.presenter = presenter
        
        testTransactionCoreObject = TestTransactionCoreObject()
    }
    
    func testOnViewDidLoad() {
        //Given
        let transactions = [
            Transaction(amount: Amount(Int.random(in: 1..<400)), type: .expense, transactionDescription: "1 item"),
            Transaction(amount: Amount(Int.random(in: 1..<400)), type: .expense, transactionDescription: "2 item"),
            Transaction(amount: Amount(Int.random(in: 1..<400)), type: .income, transactionDescription: "3 item"),
            Transaction(amount: Amount(Int.random(in: 1..<400)), type: .income, transactionDescription: "4 item"),
            Transaction(amount: Amount(Int.random(in: 1..<400)), type: .income, transactionDescription: "5 item")
        ]
        
        let expense = transactions[0].amount.integerValue + transactions[1].amount.integerValue
        let income = transactions[2].amount.integerValue + transactions[3].amount.integerValue + transactions[4].amount.integerValue
        
        let localBalance = Balance(income: income, expense: expense)
        
        let convertedManagedObjects = testTransactionCoreObject.saveTransactions(transactions)
        
        coreDataManagerMock.retrieveDataForClosure = { Type in
            self.coreDataManagerMock.delegate?.coreDataManager(self.coreDataManagerMock, didRetrieveData: convertedManagedObjects)
        }
        
        //When
        presenter.onViewDidLoad()
        
        //Then
        let viewRecievedOrderedTransactionList = view.updateViewWithReceivedViewState?.orderedTransactionList
        XCTAssert(view.updateViewWithCallsCount == 1)
        XCTAssert(viewRecievedOrderedTransactionList?.orderedTransactions.count == 1)
        XCTAssert(viewRecievedOrderedTransactionList?.orderedTransactions[0].count == transactions.count)
        
        XCTAssert(viewRecievedOrderedTransactionList?.balance.income == localBalance.income)
        XCTAssert(viewRecievedOrderedTransactionList?.balance.expense == localBalance.expense)
        XCTAssert(viewRecievedOrderedTransactionList?.balance.balance == localBalance.balance)
        
        for transaction in transactions {
            XCTAssert(viewRecievedOrderedTransactionList!.orderedTransactions[0].contains(transaction))
        }
    }

    func testOnAddButtonPressed() {
        //Given
        
        //When
        presenter.onAddButtonPressed()
        
        //Then
        XCTAssert(wireframe.openAddTransactionCallsCount == 1)
    }

    func testOnRemove() {
        // Given
        let transactions = [
            Transaction(amount: Amount(Int.random(in: 1..<400)), transactionDescription: "1 item"),
            Transaction(amount: Amount(Int.random(in: 1..<400)), transactionDescription: "2 item"),
            Transaction(amount: Amount(Int.random(in: 1..<400)), transactionDescription: "3 item"),
            Transaction(amount: Amount(Int.random(in: 1..<400)), transactionDescription: "4 item"),
            Transaction(amount: Amount(Int.random(in: 1..<400)), transactionDescription: "5 item")
        ]
        let convertedManagedObjects = testTransactionCoreObject.saveTransactions(transactions)
        
        coreDataManagerMock.retrieveDataForClosure = { Type in
            self.coreDataManagerMock.delegate?.coreDataManager(self.coreDataManagerMock, didRetrieveData: convertedManagedObjects)
        }
        
        presenter.onViewDidLoad()
        
        let recievedOrderedTransactions = view.updateViewWithReceivedViewState!.orderedTransactionList.orderedTransactions
        
        let transactionToRemove = recievedOrderedTransactions[0][0]
        
        coreDataManagerMock.removeTransactionClosure = { transaction in
            self.coreDataManagerMock.delegate?.coreDataManager(self.coreDataManagerMock, didRemoveData: transactionToRemove)
        }
        // When
        
        presenter.onRemove(transaction: transactionToRemove)
        // Then
        
        XCTAssert(view.updateViewWithCallsCount == 2)
        XCTAssert(view.updateViewWithReceivedViewState?.orderedTransactionList.orderedTransactions.count == 1)
        XCTAssert(view.updateViewWithReceivedViewState?.orderedTransactionList.orderedTransactions[0].count == transactions.count - 1)
        
        let newRecievedTransactions = view.updateViewWithReceivedViewState!.orderedTransactionList.orderedTransactions
        
        XCTAssertFalse(newRecievedTransactions[0].contains(transactionToRemove))
        
        //Given
        let transactionToRemove2 = newRecievedTransactions[0][0]
        
        coreDataManagerMock.removeTransactionClosure = { transaction in
            self.coreDataManagerMock.delegate?.coreDataManager(self.coreDataManagerMock, didFailRemoveData: transactionToRemove2)
        }
        // When
        
        presenter.onRemove(transaction: transactionToRemove2)
        // Then
        
        XCTAssert(view.presentAlertFailedToRemoveTransactionTransactionCallsCount == 1)
    }
}
