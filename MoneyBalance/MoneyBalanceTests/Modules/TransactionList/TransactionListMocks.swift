//
//  TransactionListMocks.swift
//  MoneyBalanceTests
//
//  Created by Arnold Leitli on 2022. 03. 06..
//

import UIKit
@testable import MoneyBalance

class TransactionListViewProtocolMock: TransactionListViewProtocol {
    var presenter: TransactionListPresenterProtocol!

    //MARK: - updateView

    var updateViewWithCallsCount = 0
    var updateViewWithCalled: Bool {
        return updateViewWithCallsCount > 0
    }
    var updateViewWithReceivedViewState: TransactionListViewState?
    var updateViewWithReceivedInvocations: [TransactionListViewState] = []
    var updateViewWithClosure: ((TransactionListViewState) -> Void)?

    func updateView(with viewState: TransactionListViewState) {
        updateViewWithCallsCount += 1
        updateViewWithReceivedViewState = viewState
        updateViewWithReceivedInvocations.append(viewState)
        updateViewWithClosure?(viewState)
    }

    //MARK: - presentAlertFailedToRemoveTransaction

    var presentAlertFailedToRemoveTransactionTransactionCallsCount = 0
    var presentAlertFailedToRemoveTransactionTransactionCalled: Bool {
        return presentAlertFailedToRemoveTransactionTransactionCallsCount > 0
    }
    var presentAlertFailedToRemoveTransactionTransactionReceivedTransaction: Transaction?
    var presentAlertFailedToRemoveTransactionTransactionReceivedInvocations: [Transaction] = []
    var presentAlertFailedToRemoveTransactionTransactionClosure: ((Transaction) -> Void)?

    func presentAlertFailedToRemoveTransaction(_ transaction: Transaction) {
        presentAlertFailedToRemoveTransactionTransactionCallsCount += 1
        presentAlertFailedToRemoveTransactionTransactionReceivedTransaction = transaction
        presentAlertFailedToRemoveTransactionTransactionReceivedInvocations.append(transaction)
        presentAlertFailedToRemoveTransactionTransactionClosure?(transaction)
    }

}


class TransactionListWireframeProtocolMock: TransactionListWireframeProtocol {
    var view: (TransactionListViewProtocol & UIViewController)!
    var delegate: TransactionListModuleDelegate?

    //MARK: - openAddTransaction

    var openAddTransactionCallsCount = 0
    var openAddTransactionCalled: Bool {
        return openAddTransactionCallsCount > 0
    }
    var openAddTransactionClosure: (() -> Void)?

    func openAddTransaction() {
        openAddTransactionCallsCount += 1
        openAddTransactionClosure?()
    }

    //MARK: - transactionListView

    var transactionListViewCallsCount = 0
    var transactionListViewCalled: Bool {
        return transactionListViewCallsCount > 0
    }
    var transactionListViewReturnValue: UIViewController!
    var transactionListViewClosure: (() -> UIViewController)?

    func transactionListView() -> UIViewController {
        transactionListViewCallsCount += 1
        return transactionListViewClosure.map({ $0() }) ?? transactionListViewReturnValue
    }

}
