//
//  AddTransactionModuleTests.swift
//  MoneyBalanceTests
//
//  Created by Arnold Leitli on 2022. 03. 06..
//

import XCTest
@testable import MoneyBalance

class AddTransactionModuleTests: XCTestCase {
    var wireframe: AddTransactionWireframeProtocolMock!
    var interactor: AddTransactionInteractor!
    var presenter: AddTransactionPresenter!
    var view: AddTransactionViewProtocolMock!
    
    var observers: Observer!
    var coreDataManagerMock: CoreDataManagerTransactionProtocolMock!
    var transactionManager: TransactionManager!
    
    override func setUp() {
        coreDataManagerMock = CoreDataManagerTransactionProtocolMock()
        observers = Observer(
            dispatchQueue: DispatchQueue(label: "com.leitli.moneyBalance.addTransactionModuleTests")
        )
        
        transactionManager = TransactionManager(
            coreDataManager: coreDataManagerMock,
            observers: observers
        )
        wireframe = AddTransactionWireframeProtocolMock()
        interactor = AddTransactionInteractor(transactionManager: transactionManager)
        presenter = AddTransactionPresenter()
        interactor.presenter = presenter
        presenter.interactor = interactor
        presenter.wireframe = wireframe

        view = AddTransactionViewProtocolMock()
        presenter.view = view
        view.presenter = presenter
    }

    func testOnViewDidLoad() {
        //Given
        
        //When
        presenter.onViewDidLoad()
        //Then
        
        XCTAssert(view.updateViewWithCallsCount == 1)
        XCTAssert(view.updateViewWithReceivedViewState?.transactionDescription == nil)
        XCTAssert(view.updateViewWithReceivedViewState?.amount.isZero ?? false)
        XCTAssert(view.updateViewWithReceivedViewState?.amount.currencyString == Amount().currencyString)
    }
    
    func testOnDescriptionTextBeginEditing() {
        //Given
        
        //When
        presenter.onDescriptionTextBeginEditing()
        //Then
        
        XCTAssert(view.updateViewWithReceivedViewState?.transactionDescription == "")
        XCTAssert(view.updateViewWithCallsCount == 1)
    }
    
    func testOnDescriptionTextEndEditing() {
        //Given
        
        //When
        presenter.onDescriptionTextEndEditing("")
        //Then
        
        XCTAssert(view.updateViewWithReceivedViewState?.transactionDescription == nil)
        XCTAssert(view.updateViewWithCallsCount == 1)
    }
    
    func testOnDescriptionTextEditing() {
        //Given
        
        //When
        presenter.onDescriptionTextBeginEditing()
        //Then
        
        XCTAssert(view.updateViewWithReceivedViewState?.transactionDescription == "")
        XCTAssert(view.updateViewWithCallsCount == 1)
        
        //Given
        let text = "First words"
        
        //When
        presenter.onDescriptionTextDidChange(text)
        
        //Then
        
        XCTAssert(view.updateViewWithCallsCount == 1)
        
        //When
        presenter.onDescriptionTextEndEditing(text)
        
        //Then
        XCTAssert(view.updateViewWithCallsCount == 1)
        
        //When
        presenter.onAmountClear()
        
        //Then
        XCTAssert(view.updateViewWithCallsCount == 2)
        XCTAssert(view.updateViewWithReceivedViewState?.transactionDescription == text)
    }
    
    func testAmountChanges() {
        //Given
        let numbers = ["0","0","0","5", "6", "7", "8", "9", "1", "2", "0", "2" ]
        let firstNumber = 567891202
        let firstAmountString = "$5678912.02"
        
        //When
        for i in 0..<numbers.count {
            presenter.onAmountChanged(numbers[i])
            XCTAssert(view.updateViewWithCallsCount == i + 1)
        }
        
        //Then
        XCTAssert((view.updateViewWithReceivedViewState?.amount.integerValue ?? Int.min) == firstNumber)
        XCTAssert(view.updateViewWithReceivedViewState?.amountString == firstAmountString)
        
        //Given
        let secondNumber = 5678912
        let secondAmountString = "$56789.12"
        
        //When
        for i in 0..<2 {
            presenter.onRemoveAmount()
            XCTAssert(view.updateViewWithCallsCount == numbers.count + i + 1)
        }
        
        //Then
        XCTAssert((view.updateViewWithReceivedViewState?.amount.integerValue ?? Int.min) == secondNumber)
        XCTAssert(view.updateViewWithReceivedViewState?.amountString == secondAmountString)
        
        //Given
        let thirdNumber = 0
        let thirdAmountString = "$0.00"
        
        //When
        presenter.onAmountClear()
        
        //Then
        XCTAssert(view.updateViewWithCallsCount == numbers.count + 2 + 1)
        XCTAssert((view.updateViewWithReceivedViewState?.amount.integerValue ?? Int.min) == thirdNumber)
        XCTAssert(view.updateViewWithReceivedViewState?.amountString == thirdAmountString)
        
    }
    
    func testOnCloseButtonPressed() {
        //Given The description NO empty, and the amount is clear
        let changedDescriptionText = "desciptionText"
        presenter.onDescriptionTextDidChange(changedDescriptionText)
        
        //When
        presenter.onCloseButtonPressed()
        
        //Then
        XCTAssert(view.presentAlertDirtyAddTransactionCallsCount == 1)
        
        //Given The description NO empty, and the amount is NO clear
        let amountChanged = "5"
        presenter.onAmountChanged(amountChanged)
        
        //When
        presenter.onCloseButtonPressed()
        
        //Then
        XCTAssert(view.presentAlertDirtyAddTransactionCallsCount == 2)
        
        //Given The description NO empty, and the amount is clear
        presenter.onAmountClear()
        
        //When
        presenter.onCloseButtonPressed()
        
        //Then
        XCTAssert(view.presentAlertDirtyAddTransactionCallsCount == 3)
        
        //Given The description empty, and the amount is clear
        presenter.onDescriptionTextEndEditing("")
        
        //When
        presenter.onCloseButtonPressed()
        
        //Then
        XCTAssert(wireframe.closeAddTransactionCallsCount == 1)
    }
    
    func testOnForceClose() {
        //When
        presenter.onForceClose()
        
        //Then
        XCTAssert(wireframe.closeAddTransactionCallsCount == 1)
    }
    
    func testOnAddTransactionButtonPressed() {
        //Given
        let text = "This is the description Text we push"
        let numbers = ["0","0","0","5", "6", "7", "8", "9", "1", "2", "0", "2" ]
        let changedTransactionType = TransactionType.expense
        
        presenter.onDescriptionTextDidChange(text)
        
        for i in 0..<numbers.count {
            presenter.onAmountChanged(numbers[i])
            XCTAssert(view.updateViewWithCallsCount == i + 1)
        }
        
        presenter.onTransactionTypeChanged(changedTransactionType)
        
        coreDataManagerMock.saveTransactionClosure = { transaction in
            self.coreDataManagerMock.delegate?.coreDataManager(self.coreDataManagerMock, didFailSaveData: transaction)
        }
        
        //When
        presenter.onAddTransactionButtonPressed()
        
        //Then
        XCTAssert(view.presentAlertDidFailToAddTransactionCallsCount == 1)
        
        //Given
        coreDataManagerMock.saveTransactionClosure = { transaction in
            self.coreDataManagerMock.delegate?.coreDataManager(self.coreDataManagerMock, didSaveData: transaction)
        }
        
        //When
        presenter.onAddTransactionButtonPressed()
        
        //Then
        XCTAssert(wireframe.closeAddTransactionCallsCount == 1)
    }
}
