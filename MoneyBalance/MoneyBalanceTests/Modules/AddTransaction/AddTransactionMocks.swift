//
//  AddTransactionMocks.swift
//  MoneyBalanceTests
//
//  Created by Arnold Leitli on 2022. 03. 06..
//

import UIKit
@testable import MoneyBalance

class AddTransactionWireframeProtocolMock: AddTransactionWireframeProtocol {
    var view: (AddTransactionViewProtocol & UIViewController)!
    var delegate: AddTransactionModuleDelegate?

    //MARK: - closeAddTransaction

    var closeAddTransactionCallsCount = 0
    var closeAddTransactionCalled: Bool {
        return closeAddTransactionCallsCount > 0
    }
    var closeAddTransactionClosure: (() -> Void)?

    func closeAddTransaction() {
        closeAddTransactionCallsCount += 1
        closeAddTransactionClosure?()
    }

    //MARK: - addTransactionView

    var addTransactionViewCallsCount = 0
    var addTransactionViewCalled: Bool {
        return addTransactionViewCallsCount > 0
    }
    var addTransactionViewReturnValue: UIViewController!
    var addTransactionViewClosure: (() -> UIViewController)?

    func addTransactionView() -> UIViewController {
        addTransactionViewCallsCount += 1
        return addTransactionViewClosure.map({ $0() }) ?? addTransactionViewReturnValue
    }
}

class AddTransactionViewProtocolMock: AddTransactionViewProtocol {
    var presenter: AddTransactionPresenterProtocol!

    //MARK: - updateView

    var updateViewWithCallsCount = 0
    var updateViewWithCalled: Bool {
        return updateViewWithCallsCount > 0
    }
    var updateViewWithReceivedViewState: AddTransactionViewState?
    var updateViewWithReceivedInvocations: [AddTransactionViewState] = []
    var updateViewWithClosure: ((AddTransactionViewState) -> Void)?

    func updateView(with viewState: AddTransactionViewState) {
        updateViewWithCallsCount += 1
        updateViewWithReceivedViewState = viewState
        updateViewWithReceivedInvocations.append(viewState)
        updateViewWithClosure?(viewState)
    }

    //MARK: - presentAlertDirtyAddTransaction

    var presentAlertDirtyAddTransactionCallsCount = 0
    var presentAlertDirtyAddTransactionCalled: Bool {
        return presentAlertDirtyAddTransactionCallsCount > 0
    }
    var presentAlertDirtyAddTransactionClosure: (() -> Void)?

    func presentAlertDirtyAddTransaction() {
        presentAlertDirtyAddTransactionCallsCount += 1
        presentAlertDirtyAddTransactionClosure?()
    }

    //MARK: - presentAlertDidFailToAddTransaction

    var presentAlertDidFailToAddTransactionCallsCount = 0
    var presentAlertDidFailToAddTransactionCalled: Bool {
        return presentAlertDidFailToAddTransactionCallsCount > 0
    }
    var presentAlertDidFailToAddTransactionClosure: (() -> Void)?

    func presentAlertDidFailToAddTransaction() {
        presentAlertDidFailToAddTransactionCallsCount += 1
        presentAlertDidFailToAddTransactionClosure?()
    }

}
