//
//  AmountTests.swift
//  MoneyBalanceTests
//
//  Created by Arnold Leitli on 2022. 03. 06..
//

@testable import MoneyBalance
import XCTest

class AmountTests: XCTestCase {
    var sut = Amount()

    override func setUp() {}

    func testAmountChanges() {
        // Given
        let numbers = [
            "0",
            "0",
            "0",
            "5",
            "6",
            "7",
            "8",
            "9",
            "1",
            "2",
            "0",
            "2",
        ]
        let integers = [
            0,
            0,
            0,
            5,
            56,
            567,
            5678,
            56789,
            567891,
            5678912,
            56789120,
            567891202,
            56789120,
            5678912,
            0,
        ]

        let strings = [
            "$0.00", "$0.00", "$0.00", "$0.05", "$0.56", "$5.67", "$56.78",
            "$567.89", "$5678.91", "$56789.12", "$567891.20", "$5678912.02",
            "$567891.20", "$56789.12", "$0.00",
        ]

        //When
        for i in 0 ..< strings.count {
            if i < numbers.count {
                let number = numbers[i]
                sut.append(value: number)
            } else if i < strings.count - 1 {
                sut.remove()
            } else {
                sut.clear()
            }

            //Then
            XCTAssert(sut.integerValue == integers[i])
            XCTAssert(sut.currencyString == strings[i])
        }
    }
}
