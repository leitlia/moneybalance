//
//  SceneDelegate.swift
//  MoneyBalance
//
//  Created by Arnold Leitli on 2022. 03. 01..
//

import UIKit

class SceneDelegate: UIResponder, UIWindowSceneDelegate {

    var window: UIWindow?
    
    let applicationLifeCycleEventProvider: AppLifeCycleEventProviderProtocol
    var mainModule: MainModuleProtocol
    
    override init() {
        self.applicationLifeCycleEventProvider = AppLifeCycleEventProvider(observers: Observer(dispatchQueue: DispatchQueue(label: "com.leitli.moneyBalance.appLifeCycleEventProvider")))
        self.mainModule = MainWireframe(applicationLifeCycleEventProvider: applicationLifeCycleEventProvider)
        
        super.init()
    }

    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
        
        guard let windowScene = (scene as? UIWindowScene) else { return }

        let window = UIWindow(windowScene: windowScene)
        self.window = window
        
        self.window?.rootViewController = mainModule.mainView()
        self.window?.makeKeyAndVisible()
    }

    func sceneDidDisconnect(_ scene: UIScene) {
        // Called as the scene is being released by the system.
        // This occurs shortly after the scene enters the background, or when its session is discarded.
        // Release any resources associated with this scene that can be re-created the next time the scene connects.
        // The scene may re-connect later, as its session was not necessarily discarded (see `application:didDiscardSceneSessions` instead).
    }

    func sceneDidBecomeActive(_ scene: UIScene) {
        // Called when the scene has moved from an inactive state to an active state.
        // Use this method to restart any tasks that were paused (or not yet started) when the scene was inactive.
    }

    func sceneWillResignActive(_ scene: UIScene) {
        // Called when the scene will move from an active state to an inactive state.
        // This may occur due to temporary interruptions (ex. an incoming phone call).
    }

    func sceneWillEnterForeground(_ scene: UIScene) {
        // Called as the scene transitions from the background to the foreground.
        // Use this method to undo the changes made on entering the background.
        applicationLifeCycleEventProvider.sceneWillEnterForeground()
    }

    func sceneDidEnterBackground(_ scene: UIScene) {
        // Called as the scene transitions from the foreground to the background.
        // Use this method to save data, release shared resources, and store enough scene-specific state information
        // to restore the scene back to its current state.

        // Save changes in the application's managed object context when the application transitions to the background.
        applicationLifeCycleEventProvider.sceneDidEnterBackground()
//        (UIApplication.shared.delegate as? AppDelegate)?.saveContext
    }


}

