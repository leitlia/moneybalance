//
//  File.swift
//  MoneyBalance
//
//  Created by Arnold Leitli on 2022. 03. 05..
//

import UIKit

extension UIViewController {
    func presentAlert(
        title: String? = nil,
        message: String? = nil,
        cancelActionTitle: String? = nil,
        cancelAction: (() -> Void)? = nil,
        okActionTitle: String? = nil,
        okAction: (() -> Void)? = nil
    ) {
        let alert = UIAlertController(
            title: title,
            message: message,
            preferredStyle: .alert
        )

        alert.addAction(UIAlertAction(
            title: cancelActionTitle,
            style: .cancel,
            handler: { _ in
                cancelAction?()
            }
        ))
        alert.addAction(UIAlertAction(
            title: okActionTitle,
            style: .default,
            handler: { _ in
                okAction?()
            }
        ))

        present(alert, animated: true)
    }
}
