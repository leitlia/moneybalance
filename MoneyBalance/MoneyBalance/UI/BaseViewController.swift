//
//  Balance.swift
//  MoneyBalance
//
//  Created by Arnold Leitli on 2022. 03. 05..
//

import UIKit

class BaseViewController: UIViewController {
    static var storyboardName: String {
        return String(describing: self).components(separatedBy: "ViewController")[0]
    }

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        // logAnalytics.setScreen(title ?? "no screen title", className: String(describing: type(of: self)))
    }

    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
    }

    func add(child viewController: UIViewController, to containerView: UIView) {
        add(child: viewController, to: containerView, forViewToCover: containerView)
    }

    func add(child viewController: UIViewController, to containerView: UIView, forViewToCover coveredView: UIView) {
        guard let childView = viewController.view else { return }
        viewController.willMove(toParent: self)
        childView.frame = coveredView.frame
        containerView.addSubview(childView)
        addChild(viewController)
        viewController.didMove(toParent: self)

        addConstraints(for: childView, over: coveredView)
    }

    func addConstraints(for view: UIView, over coveredView: UIView) {
        view.translatesAutoresizingMaskIntoConstraints = false
        view.topAnchor.constraint(equalTo: coveredView.topAnchor).isActive = true
        view.bottomAnchor.constraint(equalTo: coveredView.bottomAnchor).isActive = true
        view.leftAnchor.constraint(equalTo: coveredView.leftAnchor).isActive = true
        view.rightAnchor.constraint(equalTo: coveredView.rightAnchor).isActive = true
    }

    func remove() {
        guard let childView = self.view else { return }

        childView.removeConstraints(childView.constraints)
        childView.removeFromSuperview()
        removeFromParent()
    }
}
