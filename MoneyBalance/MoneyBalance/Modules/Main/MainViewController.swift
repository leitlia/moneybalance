//
//  MainViewController.swift
//  MoneyBalance
//
//  Created by Arnold Leitli on 2022. 03. 01..
//

import UIKit

// MARK: - MainViewController

final class MainViewController: BaseViewController {
    var presenter: MainPresenterProtocol!
    
    private var rootViewController: UIViewController?

    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.onViewDidLoad()
    }
}

// MARK: MainViewProtocol

extension MainViewController: MainViewProtocol {
    func setupView(rootView: UIViewController) {
        rootViewController = rootView
    }

    func buildUpView() {
        guard let rootView = rootViewController else {
            return
        }
        add(child: rootView, to: view)
    }
}
