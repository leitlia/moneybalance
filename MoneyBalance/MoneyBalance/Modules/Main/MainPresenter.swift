//
//  MainPresenter.swift
//  MoneyBalance
//
//  Created by Arnold Leitli on 2022. 03. 01..
//

import Foundation

// MARK: - MainPresenter

final class MainPresenter {
    weak var view: MainViewProtocol?
    weak var wireframe: MainWireframeProtocol?
    var interactor: MainInteractorInputProtocol!
}

// MARK: MainPresenterProtocol

extension MainPresenter: MainPresenterProtocol {
    func onViewDidLoad() {
        view?.buildUpView()
    }
}

// MARK: MainInteractorOutputProtocol

extension MainPresenter: MainInteractorOutputProtocol {}
