//
//  MainWireframe.swift
//  MoneyBalance
//
//  Created by Arnold Leitli on 2022. 03. 01..
//

import UIKit
import CoreData

final class MainWireframe {
    var view: (MainViewProtocol & UIViewController)!
    
    weak var delegate: MainModuleDelegate?
    
    private var transactionListModule: TransactionListModuleProtocol?
    
    private lazy var persistentContainer: NSPersistentContainer = {
        let container = NSPersistentContainer(name: "MoneyBalance")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    private var coreDataManager: (CoreDataManagerProtocol & CoreDataManagerTransactionProtocol)?
    private var transactionManager: TransactionManagerProtocol?
    private let applicationLifeCycleEventProvider: AppLifeCycleEventProviderProtocol
    
    init(applicationLifeCycleEventProvider: AppLifeCycleEventProviderProtocol) {
        self.applicationLifeCycleEventProvider = applicationLifeCycleEventProvider
        let interactor = MainInteractor()
        let presenter = MainPresenter()
        interactor.presenter = presenter
        presenter.interactor = interactor
        presenter.wireframe = self
        
        let storyboard = UIStoryboard(name: MainViewController.storyboardName, bundle: nil)
        view = (storyboard.instantiateViewController(withIdentifier: String(describing: MainViewController.self)) as! UIViewController & MainViewProtocol)
        presenter.view = view
        view.presenter = presenter
        
        setupDependencies()
        setupRootView()
    }
}

extension MainWireframe: MainWireframeProtocol {
}

extension MainWireframe: MainModuleProtocol {
    func mainView() -> UIViewController {
        return view
    }
}

extension MainWireframe {
    private func setupRootView() {
        guard let transactionManager = transactionManager else { return }
        transactionListModule = TransactionListWireframe(transactionManager: transactionManager)
        
        if let transactionListView = transactionListModule?.transactionListView() {
            let navigationController = BaseNavigationController(rootViewController: transactionListView)
            view.setupView(rootView: navigationController)
        }
    }
    
    private func setupDependencies() {
        coreDataManager = CoreDataManager(
            context: persistentContainer.viewContext,
            applicationLifeCycleEventProvider: applicationLifeCycleEventProvider
        )
        if let coreDataManager = coreDataManager {
            transactionManager = TransactionManager(
                coreDataManager: coreDataManager,
                observers: Observer(dispatchQueue: DispatchQueue(label: "com.leitli.moneyBalance.transactionManager"))
            )
        }
    }
}
