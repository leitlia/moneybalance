//
//  MainProtocols.swift
//  MoneyBalance
//
//  Created by Arnold Leitli on 2022. 03. 01..
//

import UIKit

// MARK: - MainInteractorInputProtocol

protocol MainInteractorInputProtocol: AnyObject {
    var presenter: MainPresenterProtocol? { get set }
}

// MARK: - MainInteractorOutputProtocol

protocol MainInteractorOutputProtocol: AnyObject {}

// MARK: - MainPresenterProtocol

protocol MainPresenterProtocol: MainInteractorOutputProtocol {
    var view: MainViewProtocol? { get set }
    var wireframe: MainWireframeProtocol? { get set }
    var interactor: MainInteractorInputProtocol! { get set }

    func onViewDidLoad()
}

// MARK: - MainViewProtocol

protocol MainViewProtocol: AnyObject {
    var presenter: MainPresenterProtocol! { get set }
    func setupView(rootView: UIViewController)
    func buildUpView()
}

// MARK: - MainWireframeProtocol

protocol MainWireframeProtocol: MainModuleProtocol {
    var view: (MainViewProtocol & UIViewController)! { get set }
}

// MARK: - MainModuleProtocol

protocol MainModuleProtocol: AnyObject {
    var delegate: MainModuleDelegate? { get set }

    func mainView() -> UIViewController
}

// MARK: - MainModuleDelegate

protocol MainModuleDelegate: AnyObject {}
