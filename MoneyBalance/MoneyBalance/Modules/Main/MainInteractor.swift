//
//  MainInteractor.swift
//  MoneyBalance
//
//  Created by Arnold Leitli on 2022. 03. 01..
//

import Foundation

// MARK: - MainInteractor

final class MainInteractor {
    var presenter: MainPresenterProtocol?
    
    init() {}
}

// MARK: MainInteractorInputProtocol

extension MainInteractor: MainInteractorInputProtocol {}
