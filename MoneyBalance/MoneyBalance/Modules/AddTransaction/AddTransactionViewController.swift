//
//  AddTransactionViewController.swift
//  MoneyBalance
//
//  Created by Arnold Leitli on 2022. 03. 01..
//

import UIKit

// MARK: - AddTransactionViewController

final class AddTransactionViewController: BaseViewController {
    var presenter: AddTransactionPresenterProtocol!
    
    private var closeBarButtonItem = UIBarButtonItem(
        title: "Close",
        style: .done,
        target: self,
        action: #selector(closeBarButtonPressed(_:))
    )

    @IBOutlet private var addButton: UIButton!
    @IBOutlet private var scrollView: UIScrollView!
    @IBOutlet private var stackView: UIStackView!
    @IBOutlet private var transactionTypeSegmentationControl: UISegmentedControl!
    @IBOutlet private var descriptionTextView: UITextView!
    @IBOutlet private var amountTextField: UITextField!
    
    let notificationCenter = NotificationCenter.default

    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.onViewDidLoad()
        closeBarButtonItem.target = self
        addButton.layer.cornerRadius = 10.0

        title = "Add Transaction"
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(tapAway))
        view.addGestureRecognizer(tap)
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        closeBarButtonItem.tintColor = .systemBackground
        navigationItem.rightBarButtonItem = closeBarButtonItem
        
        notificationCenter.addObserver(
            self,
            selector: #selector(adjustForKeyboard),
            name: UIResponder.keyboardWillHideNotification,
            object: nil
        )
        notificationCenter.addObserver(
            self,
            selector: #selector(adjustForKeyboard),
            name: UIResponder.keyboardWillChangeFrameNotification,
            object: nil
        )
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        notificationCenter.removeObserver(self)
    }

    @IBAction func transactionTypeSegmentationControlChanged(_ sender: UISegmentedControl) {
        let isIncomeSelected = sender.selectedSegmentIndex == 0
        let transactionType: TransactionType = isIncomeSelected ? .income : .expense
        presenter.onTransactionTypeChanged(transactionType)
    }

    @IBAction func addButtonPressed(_ sender: Any) {
        presenter.onAddTransactionButtonPressed()
    }

    @objc func closeBarButtonPressed(_ sender: AnyObject) {
        presenter.onCloseButtonPressed()
    }
    
    @objc func adjustForKeyboard(notification: Notification) {
        let isKeyBoardWillHideEvent = notification.name == UIResponder.keyboardWillHideNotification
        let isTextViewEditing = descriptionTextView.isFirstResponder
        if isKeyBoardWillHideEvent {
            scrollView.contentOffset = .zero
        } else {
            let inset = isTextViewEditing ? descriptionTextView.frame.maxY: amountTextField.frame.maxY
            scrollView.setContentOffset(
                CGPoint(
                    x: 0.0,
                    y: inset
                ),
                animated: true
            )
        }
    }
    
    @objc func tapAway() {
        view.endEditing(true)
    }
}

// MARK: AddTransactionViewProtocol

extension AddTransactionViewController: AddTransactionViewProtocol {
    func updateView(with viewState: AddTransactionViewState) {
        amountTextField.text = viewState.amountString
        descriptionTextView.text = viewState.transactionDescription ?? "Transaction Description"
        addButton.isEnabled = viewState.isAddButtonEnabled
    }

    func presentAlertDirtyAddTransaction() {
        presentAlert(
            title: "There are changes",
            message: "Do you want to cancel",
            cancelActionTitle: "No",
            okActionTitle: "Yes",
            okAction: { [weak self] in
                guard let self = self else { return }
                self.presenter.onForceClose()
            }
        )
    }

    func presentAlertDidFailToAddTransaction() {
        presentAlert(
            title: "Save Failed",
            message: "We could not save your transaction! Do you want to try again?",
            cancelActionTitle: "Cancel",
            okActionTitle: "Try Again",
            okAction: { [weak self] in
                guard let self = self else { return }
                self.presenter.onAddTransactionButtonPressed()
            }
        )
    }
}

// MARK: UITextViewDelegate

extension AddTransactionViewController: UITextViewDelegate {
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.textColor = .systemGray3
        }
        presenter.onDescriptionTextEndEditing(textView.text)
    }

    func textViewDidBeginEditing(_ textView: UITextView) {
        textView.textColor = .label
        presenter.onDescriptionTextBeginEditing()
    }

    func textViewDidChange(_ textView: UITextView) {
        presenter.onDescriptionTextDidChange(textView.text)
    }
}

// MARK: UITextFieldDelegate

extension AddTransactionViewController: UITextFieldDelegate {
    func textField(
        _ textField: UITextField,
        shouldChangeCharactersIn range: NSRange,
        replacementString string: String
    ) -> Bool {
        if Int(string) != nil {
            presenter.onAmountChanged(string)
        } else if string.isEmpty && range.length == 1 {
            presenter.onRemoveAmount()
        }
        return false
    }
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        presenter.onAmountClear()
        return false
    }
}
