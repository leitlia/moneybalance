//
//  AddTransactionPresenter.swift
//  MoneyBalance
//
//  Created by Arnold Leitli on 2022. 03. 01..
//

import Foundation

// MARK: - AddTransactionPresenter

final class AddTransactionPresenter {
    weak var view: AddTransactionViewProtocol?
    weak var wireframe: AddTransactionWireframeProtocol?
    var interactor: AddTransactionInteractorInputProtocol!

    private var viewState = AddTransactionViewState()
}

// MARK: AddTransactionPresenterProtocol

extension AddTransactionPresenter: AddTransactionPresenterProtocol {
    func onTransactionTypeChanged(_ transactionType: TransactionType) {
        viewState.transactionType = transactionType
    }

    func onDescriptionTextBeginEditing() {
        if viewState.transactionDescription == nil {
            viewState.transactionDescription = ""
            view?.updateView(with: viewState)
        }
    }

    func onDescriptionTextEndEditing(_ text: String) {
        if text.isEmpty {
            viewState.transactionDescription = nil
            view?.updateView(with: viewState)
        }
    }

    func onDescriptionTextDidChange(_ text: String) {
        viewState.transactionDescription = text
    }

    func onAmountChanged(_ text: String) {
        viewState.amount.append(value: text)
        viewState.amountString = viewState.amount.currencyString
        view?.updateView(with: viewState)
    }

    func onRemoveAmount() {
        viewState.amount.remove()
        viewState.amountString = viewState.amount.currencyString
        view?.updateView(with: viewState)
    }

    func onAmountClear() {
        viewState.amount.clear()
        viewState.amountString = viewState.amount.currencyString
        view?.updateView(with: viewState)
    }

    func onCloseButtonPressed() {
        if viewState.isDirty {
            view?.presentAlertDirtyAddTransaction()
        } else {
            wireframe?.closeAddTransaction()
        }
    }

    func onForceClose() {
        wireframe?.closeAddTransaction()
    }

    func onAddTransactionButtonPressed() {
        let transaction = Transaction(
            uuid: UUID().uuidString,
            amount: viewState.amount,
            type: viewState.transactionType,
            transactionDescription: viewState.transactionDescription ?? "N/A",
            createdAt: Date()
        )
        interactor.addTransaction(transaction)
    }

    func onViewDidLoad() {
        viewState.amount.clear()
        viewState.amountString = viewState.amount.currencyString
        view?.updateView(with: viewState)
    }
}

// MARK: AddTransactionInteractorOutputProtocol

extension AddTransactionPresenter: AddTransactionInteractorOutputProtocol {
    func didAddTransaction() {
        wireframe?.closeAddTransaction()
    }

    func didFailToAddTransaction() {
        view?.presentAlertDidFailToAddTransaction()
    }
}
