//
//  AddTransactionProtocols.swift
//  MoneyBalance
//
//  Created by Arnold Leitli on 2022. 03. 01..
//

import UIKit

// MARK: - AddTransactionInteractorInputProtocol

protocol AddTransactionInteractorInputProtocol: AnyObject {
    var presenter: AddTransactionPresenterProtocol? { get set }

    func addTransaction(_ transaction: Transaction)
}

// MARK: - AddTransactionInteractorOutputProtocol

protocol AddTransactionInteractorOutputProtocol: AnyObject {
    func didAddTransaction()
    func didFailToAddTransaction()
}

// MARK: - AddTransactionPresenterProtocol

protocol AddTransactionPresenterProtocol: AddTransactionInteractorOutputProtocol {
    var view: AddTransactionViewProtocol? { get set }
    var wireframe: AddTransactionWireframeProtocol? { get set }
    var interactor: AddTransactionInteractorInputProtocol! { get set }

    func onViewDidLoad()
    func onTransactionTypeChanged(_ transactionType: TransactionType)
    func onDescriptionTextBeginEditing()
    func onDescriptionTextEndEditing(_ text: String)
    func onDescriptionTextDidChange(_ text: String)
    func onAmountChanged(_ text: String)
    func onRemoveAmount()
    func onAmountClear()
    func onAddTransactionButtonPressed()
    func onCloseButtonPressed()
    func onForceClose()
}

// MARK: - AddTransactionViewState

struct AddTransactionViewState {
    var transactionType = TransactionType.income
    var transactionDescription: String?
    var amount = Amount()
    var amountString = ""
    var isAddButtonEnabled: Bool { !amount.isZero }
    var isDirty: Bool {
        transactionDescription != nil || !amount.isZero
    }

    private let numberFormatter = CurrencyStringFormatter()
}

// MARK: - AddTransactionViewProtocol

protocol AddTransactionViewProtocol: AnyObject {
    var presenter: AddTransactionPresenterProtocol! { get set }

    func updateView(with viewState: AddTransactionViewState)
    func presentAlertDirtyAddTransaction()
    func presentAlertDidFailToAddTransaction()
}

// MARK: - AddTransactionWireframeProtocol

protocol AddTransactionWireframeProtocol: AddTransactionModuleProtocol {
    var view: (AddTransactionViewProtocol & UIViewController)! { get set }

    func closeAddTransaction()
}

// MARK: - AddTransactionModuleProtocol

protocol AddTransactionModuleProtocol: AnyObject {
    var delegate: AddTransactionModuleDelegate? { get set }

    func addTransactionView() -> UIViewController
}

// MARK: - AddTransactionModuleDelegate

protocol AddTransactionModuleDelegate: AnyObject {
    func closeAddTransactionModule()
}
