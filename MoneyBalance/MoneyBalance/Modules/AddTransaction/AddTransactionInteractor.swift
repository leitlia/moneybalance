//
//  AddTransactionInteractor.swift
//  MoneyBalance
//
//  Created by Arnold Leitli on 2022. 03. 01..
//

import Foundation

// MARK: - AddTransactionInteractor

final class AddTransactionInteractor {
    var presenter: AddTransactionPresenterProtocol?

    private let transactionManager: TransactionManagerProtocol

    init(transactionManager: TransactionManagerProtocol) {
        self.transactionManager = transactionManager

        transactionManager.addObserver(self)
    }

    deinit {
        transactionManager.removeObserver(self)
    }
}

// MARK: AddTransactionInteractorInputProtocol

extension AddTransactionInteractor: AddTransactionInteractorInputProtocol {
    func addTransaction(_ transaction: Transaction) {
        transactionManager.appendTransaction(transaction)
    }
}

// MARK: TransactionManagerDelegate

extension AddTransactionInteractor: TransactionManagerDelegate {
    func transactionManager(
        _ transactionManager: TransactionManagerProtocol,
        didAddTransaction transaction: Transaction,
        in transactions: [Transaction]
    ) {
        presenter?.didAddTransaction()
    }

    func transactionManager(
        _ transactionManager: TransactionManagerProtocol,
        didFailToAppendTransaction transaction: Transaction,
        error: TransactionError
    ) {
        presenter?.didFailToAddTransaction()
    }
}
