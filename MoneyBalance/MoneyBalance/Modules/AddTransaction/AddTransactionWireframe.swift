//
//  AddTransactionWireframe.swift
//  MoneyBalance
//
//  Created by Arnold Leitli on 2022. 03. 01..
//

import UIKit

// MARK: - AddTransactionWireframe

final class AddTransactionWireframe {
    var view: (AddTransactionViewProtocol & UIViewController)!

    weak var delegate: AddTransactionModuleDelegate?

    init(transactionManager: TransactionManagerProtocol) {
        let interactor =
            AddTransactionInteractor(transactionManager: transactionManager)
        let presenter = AddTransactionPresenter()
        interactor.presenter = presenter
        presenter.interactor = interactor
        presenter.wireframe = self

        let storyboard = UIStoryboard(
            name: AddTransactionViewController.storyboardName,
            bundle: nil
        )
        view = (storyboard.instantiateViewController(withIdentifier: String(describing: AddTransactionViewController.self)) as! UIViewController & AddTransactionViewProtocol)
        presenter.view = view
        view.presenter = presenter
    }
}

// MARK: AddTransactionWireframeProtocol

extension AddTransactionWireframe: AddTransactionWireframeProtocol {
    func closeAddTransaction() {
        delegate?.closeAddTransactionModule()
    }
}

// MARK: AddTransactionModuleProtocol

extension AddTransactionWireframe: AddTransactionModuleProtocol {
    func addTransactionView() -> UIViewController {
        return view
    }
}
