//
//  TransactionListCell.swift
//  MoneyBalance
//
//  Created by Arnold Leitli on 2022. 03. 01..
//

import UIKit

final class TransactionListCell: UITableViewCell, ReuseIdentifiable, TransactionCellSetupable {
    @IBOutlet private var containerView: UIView!
    @IBOutlet private var descriptionLabel: UILabel!
    @IBOutlet private var amountLabel: UILabel!

    private lazy var currencyFormatter = CurrencyStringFormatter()

    @IBOutlet var leadingConstraintOfDeleteDemo: NSLayoutConstraint!

    func setup(with transaction: Transaction) {
        descriptionLabel.text = transaction.transactionDescription
        let isExpense = transaction.type == .expense
        let parity = isExpense ? "- " : ""
        let amountText = currencyFormatter.currencyString(from: transaction.amount.integerValue)
        amountLabel.text = parity + amountText
        amountLabel.textColor = isExpense ? .label : .systemGreen
    }

    func animateSwipeHint() {
        slideInFromRight()
    }

    private func slideInFromRight() {
        UIView.animate(
            withDuration: 1,
            delay: 1,
            usingSpringWithDamping: 0.3,
            initialSpringVelocity: 0.2,
            options: [.curveEaseInOut, .allowUserInteraction]) {
                self.containerView.transform = CGAffineTransform(
                    translationX: -40,
                    y: 0
                )
                self.leadingConstraintOfDeleteDemo.constant = -40
                self.layoutIfNeeded()
            } completion: { success in
                UIView.animate(
                    withDuration: 1,
                    delay: 0.1,
                    usingSpringWithDamping: 1,
                    initialSpringVelocity: 1,
                    options: [.curveEaseInOut, .allowUserInteraction]
                ) {
                    self.containerView.transform = .identity
                    self.leadingConstraintOfDeleteDemo.constant = 0
                    self.layoutIfNeeded()
                }
            }
    }
}
