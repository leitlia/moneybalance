//
//  TransactionListBalanceHeaderView.swift
//  MoneyBalance
//
//  Created by Arnold Leitli on 2022. 03. 01..
//

import UIKit

final class TransactionListBalanceHeaderView: UIView {
    @IBOutlet private var containerView: UIView!
    @IBOutlet private var expenseTitleLabel: UILabel!
    @IBOutlet private var expenseAmountLabel: UILabel!
    @IBOutlet private var incomeTitleLabel: UILabel!
    @IBOutlet private var incomeAmountLabel: UILabel!
    @IBOutlet private var balanceTitleLabel: UILabel!
    @IBOutlet private var balanceAmountLabel: UILabel!

    @IBOutlet private var progressBackground: UIView!
    @IBOutlet private var progressIncome: UIView!
    @IBOutlet private var progressConstraint: NSLayoutConstraint!

    private lazy var currencyFormatter = CurrencyStringFormatter()

    override func awakeFromNib() {
        super.awakeFromNib()

        containerView.layer.cornerRadius = 10.0
        containerView.layer.borderColor = UIColor.systemGray5.cgColor
        containerView.layer.borderWidth = 2.0

        progressBackground.layer.cornerRadius = 10.0
        progressBackground.layer.borderColor = UIColor.systemTeal.cgColor
        progressBackground.layer.borderWidth = 1.0
        
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOffset = CGSize(width: 0, height: 3)
        self.layer.shadowOpacity = 0.3
        self.layer.shadowRadius = 5
    }

    func setup(with balance: Balance, animated: Bool) {
        expenseAmountLabel.text = currencyFormatter.currencyString(from: balance.expense)
        incomeAmountLabel.text = currencyFormatter.currencyString(from: balance.income)
        balanceAmountLabel.text = currencyFormatter.currencyString(from: balance.balance)

        let unit = progressBackground.frame.width / CGFloat(balance.expense + balance.income + 1)
        var expense = unit * CGFloat(balance.expense)
        expense = min(expense, progressBackground.frame.width)
        expense = max(expense, 0)

        UIView.animate(
            withDuration: animated ? 1 : 0,
            delay: 0.0,
            usingSpringWithDamping: 1,
            initialSpringVelocity: 1,
            options: [.curveEaseInOut, .allowUserInteraction]
        ) {
            self.progressConstraint.constant = expense
            self.layoutIfNeeded()
        }
    }
}
