//
//  TransactionListFirstCell.swift
//  MoneyBalance
//
//  Created by Arnold Leitli on 2022. 03. 01..
//

import UIKit

final class TransactionListFirstCell: UITableViewCell, ReuseIdentifiable, TransactionCellSetupable {
    @IBOutlet private var containerView: UIView!
    @IBOutlet private var dateLabel: UILabel!

    private var dateFormatter: DateFormatter?

    override func awakeFromNib() {
        super.awakeFromNib()
        dateFormatter = DateFormatter()
        dateFormatter?.dateStyle = .long
        dateFormatter?.timeStyle = .none
        containerView.layer.cornerRadius = 10.0
    }

    func setup(with transaction: Transaction) {
        dateLabel.text = dateFormatter?.string(from: transaction.createdAt)
    }
}
