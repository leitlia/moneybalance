//
//  TransactionListLastCell.swift
//  MoneyBalance
//
//  Created by Arnold Leitli on 2022. 03. 01..
//

import UIKit

final class TransactionListLastCell: UITableViewCell, ReuseIdentifiable, TransactionCellSetupable {
    @IBOutlet private var containerView: UIView!
    @IBOutlet private var descriptionLabel: UILabel!
    @IBOutlet private var amountLabel: UILabel!

    private lazy var currencyFormatter = CurrencyStringFormatter()

    override func awakeFromNib() {
        super.awakeFromNib()
        containerView.layer.cornerRadius = 10.0
    }

    func setup(with transaction: Transaction) {
        descriptionLabel.text = transaction.transactionDescription
        let isExpense = transaction.type == .expense
        let parity = isExpense ? "- " : ""
        let amountText = currencyFormatter.currencyString(from: transaction.amount.integerValue)
        amountLabel.text = parity + amountText
        amountLabel.textColor = isExpense ? .label : .systemGreen
    }
}
