//
//  TransactionListInteractor.swift
//  MoneyBalance
//
//  Created by Arnold Leitli on 2022. 03. 01..
//

import Foundation

// MARK: - TransactionListInteractor

final class TransactionListInteractor {
    var presenter: TransactionListPresenterProtocol?

    private let transactionManager: TransactionManagerProtocol

    init(transactionManager: TransactionManagerProtocol) {
        self.transactionManager = transactionManager

        transactionManager.addObserver(self)
    }

    deinit {
        transactionManager.removeObserver(self)
    }
}

// MARK: TransactionListInteractorInputProtocol

extension TransactionListInteractor: TransactionListInteractorInputProtocol {
    func retrieveTransactions() {
        transactionManager.retrieveTransactions()
    }

    func removeTransaction(_ transaction: Transaction) {
        transactionManager.removeTransaction(transaction)
    }
}

// MARK: TransactionManagerDelegate

extension TransactionListInteractor: TransactionManagerDelegate {
    func transactionManager(
        _ transactionManager: TransactionManagerProtocol,
        didRetrieveTransactions transactions: [Transaction]
    ) {
        presenter?.didRetrieveTransactions(transactions)
    }

    func transactionManager(
        _ transactionManager: TransactionManagerProtocol,
        didAddTransaction transaction: Transaction,
        in transactions: [Transaction]
    ) {
        presenter?.didAddNewTransaction(transaction, in: transactions)
    }

    func transactionManager(
        _ transactionManager: TransactionManagerProtocol,
        didRemoveTransaction transaction: Transaction,
        from transactions: [Transaction]
    ) {
        presenter?.didRemoveTransaction(transaction, from: transactions)
    }

    func transactionManager(
        _ transactionManager: TransactionManagerProtocol,
        didFailToRemoveTransaction transaction: Transaction,
        error: TransactionError
    ) {
        presenter?.didFailToRemoveTransaction(transaction)
    }
}
