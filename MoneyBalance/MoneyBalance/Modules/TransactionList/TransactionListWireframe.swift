//
//  TransactionListWireframe.swift
//  MoneyBalance
//
//  Created by Arnold Leitli on 2022. 03. 01..
//

import UIKit

// MARK: - TransactionListWireframe

final class TransactionListWireframe {
    var view: (TransactionListViewProtocol & UIViewController)!

    weak var delegate: TransactionListModuleDelegate?

    private var addTransactionModule: AddTransactionModuleProtocol?

    private let transactionManager: TransactionManagerProtocol

    init(transactionManager: TransactionManagerProtocol) {
        self.transactionManager = transactionManager

        let interactor = TransactionListInteractor(transactionManager: transactionManager)
        let presenter = TransactionListPresenter()
        interactor.presenter = presenter
        presenter.interactor = interactor
        presenter.wireframe = self

        let storyboard = UIStoryboard(
            name: TransactionListViewController.storyboardName,
            bundle: nil
        )
        view = (storyboard.instantiateViewController(withIdentifier: String(describing: TransactionListViewController.self)) as! UIViewController & TransactionListViewProtocol)
        presenter.view = view
        view.presenter = presenter
    }
}

// MARK: TransactionListWireframeProtocol

extension TransactionListWireframe: TransactionListWireframeProtocol {
    func openAddTransaction() {
        addTransactionModule = AddTransactionWireframe(transactionManager: transactionManager)
        addTransactionModule?.delegate = self
        guard let addTransactionView = addTransactionModule?.addTransactionView() else { return }
        let navigationController = BaseNavigationController(rootViewController: addTransactionView)
        navigationController.modalPresentationStyle = .formSheet
        view.present(navigationController, animated: true)
    }
}

// MARK: TransactionListModuleProtocol

extension TransactionListWireframe: TransactionListModuleProtocol {
    func transactionListView() -> UIViewController {
        return view
    }
}

// MARK: AddTransactionModuleDelegate

extension TransactionListWireframe: AddTransactionModuleDelegate {
    func closeAddTransactionModule() {
        addTransactionModule?.addTransactionView().dismiss(animated: true)
        addTransactionModule = nil
    }
}
