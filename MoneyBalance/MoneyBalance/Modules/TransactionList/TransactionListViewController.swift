//
//  TransactionListViewController.swift
//  MoneyBalance
//
//  Created by Arnold Leitli on 2022. 03. 01..
//

import UIKit

// MARK: - TransactionListViewController

final class TransactionListViewController: BaseViewController {
    var presenter: TransactionListPresenterProtocol!
    
    private var transactionList: [[Transaction]] = []

    @IBOutlet private var tableView: UITableView!
    @IBOutlet private var balanceHeaderView: TransactionListBalanceHeaderView!
    @IBOutlet private var bottomSpacer: UIView!

    @IBOutlet private var addTransactionButton: UIButton!
    @IBOutlet private var tooltipArrow: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.onViewDidLoad()

        title = "MonayBalance"

        addTransactionButton.layer.cornerRadius = 40.0
        addTransactionButton.layer.shadowColor = UIColor.black.cgColor
        addTransactionButton.layer.shadowOffset = CGSize(width: 3, height: 3)
        addTransactionButton.layer.shadowOpacity = 0.4
        addTransactionButton.layer.shadowRadius = 6

        bottomSpacer.layer.shadowColor = UIColor.black.cgColor
        bottomSpacer.layer.shadowOffset = CGSize(width: 0, height: -3)
        bottomSpacer.layer.shadowOpacity = 0.2
        bottomSpacer.layer.shadowRadius = 4
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        if !tableView.visibleCells.isEmpty,
           let cell = tableView.visibleCells.first(where: { cell in
               type(of: cell) == TransactionListCell.self
           }) as? TransactionListCell {
            cell.animateSwipeHint()
        }
    }

    @IBAction func addTransactionButtonPressed(_ sender: Any) {
        presenter.onAddButtonPressed()
    }
}

// MARK: TransactionListViewProtocol

extension TransactionListViewController: TransactionListViewProtocol {
    func updateView(with viewState: TransactionListViewState) {
        transactionList = viewState.orderedTransactionList.orderedTransactions
        balanceHeaderView.setup(
            with: viewState.orderedTransactionList.balance,
            animated: !viewState.firstViewLoad
        )
        UIView.transition(
            with: tableView,
            duration: 0.35,
            options: .transitionCrossDissolve,
            animations: { () -> Void in
                self.tableView.reloadData()
            },
            completion: nil
        )
        
        updateToolTipArrow(viewState.isEmptyList)
    }

    func presentAlertFailedToRemoveTransaction(_ transaction: Transaction) {
        presentAlert(
            title: "Remove Failed",
            message: "We could not remove your transaction! Do you want to try again?",
            cancelActionTitle: "Cancel",
            okActionTitle: "Try Again",
            okAction: { [weak self] in
                guard let self = self else { return }
                self.presenter.onRemove(transaction: transaction)
            }
        )
    }
}

// MARK: UITableViewDataSource

extension TransactionListViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return transactionList.count
    }

    func tableView(
        _ tableView: UITableView,
        numberOfRowsInSection section: Int
    ) -> Int {
        return transactionList[section].count + 1
    }

    func tableView(
        _ tableView: UITableView,
        cellForRowAt indexPath: IndexPath
    ) -> UITableViewCell {
        let cell = cell(from: tableView, for: indexPath)
        let index = indexPath.row == 0 ? 0 : indexPath.row - 1
        let transaction = transactionList[indexPath.section][index]
        (cell as? TransactionCellSetupable)?.setup(with: transaction)
        return cell
    }

    func tableView(
        tableView: UITableView,
        estimatedHeightForRowAtIndexPath indexPath: NSIndexPath
    ) -> CGFloat {
        return UITableView.automaticDimension
    }
}

// MARK: UITableViewDelegate

extension TransactionListViewController: UITableViewDelegate {
    func tableView(
        _ tableView: UITableView,
        trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath
    ) -> UISwipeActionsConfiguration? {
        if indexPath.row == 0 { return nil }
        let index = indexPath.row - 1
        let transaction = transactionList[indexPath.section][index]
        let action = UIContextualAction(
            style: .normal,
            title: "Delete"
        ) { [weak self] _, _, completionHandler in
            self?.presenter.onRemove(transaction: transaction)
            completionHandler(true)
        }
        action.backgroundColor = .systemRed

        return UISwipeActionsConfiguration(actions: [action])
    }
}

extension TransactionListViewController {
    private func cell(
        from tableView: UITableView,
        for indexPath: IndexPath
    ) -> UITableViewCell {
        let isFirst = indexPath.row == 0
        let isLastCell = indexPath.row == tableView.numberOfRows(inSection: indexPath.section) - 1

        if isFirst {
            return tableView.dequeueReusableCell(
                withIdentifier: TransactionListFirstCell.reuseIdentifier,
                for: indexPath
            )
        } else if isLastCell {
            return tableView.dequeueReusableCell(
                withIdentifier: TransactionListLastCell.reuseIdentifier,
                for: indexPath
            )
        }

        return tableView.dequeueReusableCell(
            withIdentifier: TransactionListCell.reuseIdentifier,
            for: indexPath
        )
    }
    
    private func updateToolTipArrow(_ shouldShowArrow: Bool) {
        tooltipArrow.isHidden = !shouldShowArrow
        
        if shouldShowArrow {
            let pulseAnimation = CABasicAnimation(keyPath: "opacity")
            pulseAnimation.duration = 1
            pulseAnimation.fromValue = 0
            pulseAnimation.toValue = 1
            pulseAnimation.timingFunction = CAMediaTimingFunction(name: .easeInEaseOut)
            pulseAnimation.autoreverses = true
            pulseAnimation.repeatCount = .greatestFiniteMagnitude
            tooltipArrow.layer.add(pulseAnimation, forKey: nil)
        } else {
            tooltipArrow.layer.removeAllAnimations()
        }
    }
}
