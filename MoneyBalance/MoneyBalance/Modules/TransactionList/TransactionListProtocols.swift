//
//  TransactionListProtocols.swift
//  MoneyBalance
//
//  Created by Arnold Leitli on 2022. 03. 01..
//

import UIKit

// MARK: - TransactionListInteractorInputProtocol

protocol TransactionListInteractorInputProtocol: AnyObject {
    var presenter: TransactionListPresenterProtocol? { get set }

    func retrieveTransactions()
    func removeTransaction(_ transaction: Transaction)
}

// MARK: - TransactionListInteractorOutputProtocol

protocol TransactionListInteractorOutputProtocol: AnyObject {
    func didRetrieveTransactions(_ transactions: [Transaction])
    func didAddNewTransaction(_ transaction: Transaction, in transactions: [Transaction])
    func didRemoveTransaction(_ transaction: Transaction, from transactions: [Transaction])
    func didFailToRemoveTransaction(_ transaction: Transaction)
}

// MARK: - TransactionListPresenterProtocol

protocol TransactionListPresenterProtocol: TransactionListInteractorOutputProtocol {
    var view: TransactionListViewProtocol? { get set }
    var wireframe: TransactionListWireframeProtocol? { get set }
    var interactor: TransactionListInteractorInputProtocol! { get set }

    func onViewDidLoad()
    func onAddButtonPressed()
    func onRemove(transaction: Transaction)
}

// MARK: - TransactionListViewState

struct TransactionListViewState {
    var firstViewLoad = true
    var orderedTransactionList: OrderedTransactionList = OrderedTransactionList(transactions: [])
    var isEmptyList: Bool { orderedTransactionList.orderedTransactions.isEmpty }
}

// MARK: - TransactionListViewProtocol

protocol TransactionListViewProtocol: AnyObject {
    var presenter: TransactionListPresenterProtocol! { get set }

    func updateView(with viewState: TransactionListViewState)
    func presentAlertFailedToRemoveTransaction(_ transaction: Transaction)
}

// MARK: - TransactionListWireframeProtocol

protocol TransactionListWireframeProtocol: TransactionListModuleProtocol {
    var view: (TransactionListViewProtocol & UIViewController)! { get set }

    func openAddTransaction()
}

// MARK: - TransactionListModuleProtocol

protocol TransactionListModuleProtocol: AnyObject {
    var delegate: TransactionListModuleDelegate? { get set }

    func transactionListView() -> UIViewController
}

// MARK: - TransactionListModuleDelegate

protocol TransactionListModuleDelegate: AnyObject {
    func closeTransactionListModule()
}
