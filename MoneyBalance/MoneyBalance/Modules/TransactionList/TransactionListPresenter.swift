//
//  TransactionListPresenter.swift
//  MoneyBalance
//
//  Created by Arnold Leitli on 2022. 03. 01..
//

import Foundation

// MARK: - TransactionListPresenter

final class TransactionListPresenter {
    weak var view: TransactionListViewProtocol?
    weak var wireframe: TransactionListWireframeProtocol?
    var interactor: TransactionListInteractorInputProtocol!

    private var viewState = TransactionListViewState()
}

// MARK: TransactionListPresenterProtocol

extension TransactionListPresenter: TransactionListPresenterProtocol {
    func onViewDidLoad() {
        interactor.retrieveTransactions()
    }

    func onAddButtonPressed() {
        wireframe?.openAddTransaction()
    }

    func onRemove(transaction: Transaction) {
        interactor.removeTransaction(transaction)
    }
}

// MARK: TransactionListInteractorOutputProtocol

extension TransactionListPresenter: TransactionListInteractorOutputProtocol {
    func didRetrieveTransactions(_ transactions: [Transaction]) {
        viewState.orderedTransactionList = OrderedTransactionList(transactions: transactions)
        view?.updateView(with: viewState)
        viewState.firstViewLoad = false
    }

    func didAddNewTransaction(_ transaction: Transaction, in transactions: [Transaction]) {
        viewState.orderedTransactionList = OrderedTransactionList(transactions: transactions)
        view?.updateView(with: viewState)
    }
    
    func didRemoveTransaction(_ transaction: Transaction, from transactions: [Transaction]) {
        viewState.orderedTransactionList = OrderedTransactionList(transactions: transactions)
        view?.updateView(with: viewState)
    }
    
    func didFailToRemoveTransaction(_ transaction: Transaction) {
        view?.presentAlertFailedToRemoveTransaction(transaction)
    }
}
