//
//  TransactionManager.swift
//  MoneyBalance
//
//  Created by Arnold Leitli on 2022. 03. 01..
//

import CoreData
import Foundation

// MARK: - TransactionManager

class TransactionManager {
    private let coreDataManager: CoreDataManagerTransactionProtocol
    private let observers: Observable

    private var transactions = [Transaction]()

    init(
        coreDataManager: CoreDataManagerTransactionProtocol,
        observers: Observable
    ) {
        self.coreDataManager = coreDataManager
        self.observers = observers
        coreDataManager.delegate = self
    }
}

// MARK: TransactionManagerProtocol

extension TransactionManager: TransactionManagerProtocol {
    func appendTransaction(_ transaction: Transaction) {
        coreDataManager.save(transaction: transaction)
    }

    func removeTransaction(_ transaction: Transaction) {
        coreDataManager.remove(transaction: transaction)
    }

    func retrieveTransactions() {
        coreDataManager.retrieveData(for: TransactionCoreObject.self)
    }

    func addObserver(_ observer: TransactionManagerDelegate) {
        observers.addObserver(observer)
    }

    func removeObserver(_ observer: TransactionManagerDelegate) {
        observers.removeObserver(observer)
    }
}

// MARK: CoreDataManagerDelegate

extension TransactionManager: CoreDataManagerDelegate {
    func coreDataManager(
        _ coreDataManager: CoreDataManagerProtocol,
        didRetrieveData data: [Any]
    ) {
        guard let transactionCoreObjects = data as? [TransactionCoreObject]
        else { return }
        transactions = transactionCoreObjects.map { coreObject in
            Transaction(
                uuid: coreObject.id ?? "",
                persistReference: coreObject.objectID,
                amount: Amount(Int(coreObject.amount)),
                type: TransactionType(rawValue: Int(coreObject.transactionType)) ?? .income,
                transactionDescription: coreObject.transactionDescription ?? "",
                createdAt: coreObject.createdAt ?? Date()
            )
        }
        notifyObserversDidRetrieveTransactions(transactions)
    }

    func coreDataManager(
        _ coreDataManager: CoreDataManagerProtocol,
        didSaveData data: Any
    ) {
        guard let transaction = data as? Transaction else { return }
        transactions.append(transaction)
        notifyObserversDidAddTransaction(transaction, transactions)
    }

    func coreDataManager(
        _ coreDataManager: CoreDataManagerProtocol,
        didFailSaveData data: Any
    ) {
        guard let transaction = data as? Transaction else { return }
        notifyObserversDidFailToAppendTransaction(transaction, error: .coreData)
    }

    func coreDataManager(
        _ coreDataManager: CoreDataManagerProtocol,
        didRemoveData data: Any
    ) {
        guard let removedTransaction = data as? Transaction else { return }
        transactions.removeAll { transaction in
            transaction.uuid == removedTransaction.uuid
        }
        notifyObserversDidRemoveTransaction(removedTransaction, transactions)
    }

    func coreDataManager(
        _ coreDataManager: CoreDataManagerProtocol,
        didFailRemoveData data: Any
    ) {
        guard let transaction = data as? Transaction else { return }
        notifyObserversDidFailToRemoveTransaction(transaction, error: .coreData)
    }
}

extension TransactionManager {
    private func notifyObserversDidRetrieveTransactions(
        _ transactions: [Transaction]
    ) {
        notify { delegate in
            delegate.transactionManager(
                self,
                didRetrieveTransactions: transactions
            )
        }
    }

    private func notifyObserversDidAddTransaction(
        _ transaction: Transaction,
        _ transactions: [Transaction]
    ) {
        notify { delegate in
            delegate.transactionManager(
                self,
                didAddTransaction: transaction,
                in: transactions
            )
        }
    }

    private func notifyObserversDidRemoveTransaction(
        _ transaction: Transaction,
        _ transactions: [Transaction]
    ) {
        notify { delegate in
            delegate.transactionManager(
                self,
                didRemoveTransaction: transaction,
                from: transactions
            )
        }
    }

    private func notifyObserversDidFailToRetrieveTransactions(
        _ error: TransactionError
    ) {
        notify { delegate in
            delegate.transactionManager(
                self,
                didFailToRetrieveTransactions: error
            )
        }
    }

    private func notifyObserversDidFailToAppendTransaction(
        _ transaction: Transaction,
        error: TransactionError
    ) {
        notify { delegate in
            delegate.transactionManager(
                self,
                didFailToAppendTransaction: transaction,
                error: error
            )
        }
    }

    private func notifyObserversDidFailToRemoveTransaction(
        _ transaction: Transaction,
        error: TransactionError
    ) {
        notify { delegate in
            delegate.transactionManager(
                self,
                didFailToRemoveTransaction: transaction,
                error: error
            )
        }
    }

    private func notify(method: (TransactionManagerDelegate) -> Void) {
        for subscriber in observers.subscribers {
            if let subscriber = subscriber as? TransactionManagerDelegate {
                method(subscriber)
            }
        }
    }
}
