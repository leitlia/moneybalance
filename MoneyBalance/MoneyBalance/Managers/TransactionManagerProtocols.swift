//
//  TransactionManagerProtocols.swift
//  MoneyBalance
//
//  Created by Arnold Leitli on 2022. 03. 05..
//

import Foundation

// MARK: - TransactionError

enum TransactionError: Error {
    case corruptMemory
    case coreData
    case server
    case unknown
}

// MARK: - TransactionManagerDelegate

protocol TransactionManagerDelegate: AnyObject {
    func transactionManager(
        _ transactionManager: TransactionManagerProtocol,
        didRetrieveTransactions transactions: [Transaction]
    )
    func transactionManager(
        _ transactionManager: TransactionManagerProtocol,
        didAddTransaction transaction: Transaction,
        in transactions: [Transaction]
    )
    func transactionManager(
        _ transactionManager: TransactionManagerProtocol,
        didRemoveTransaction transaction: Transaction,
        from transactions: [Transaction]
    )
    func transactionManager(
        _ transactionManager: TransactionManagerProtocol,
        didFailToRetrieveTransactions error: TransactionError
    )
    func transactionManager(
        _ transactionManager: TransactionManagerProtocol,
        didFailToAppendTransaction transaction: Transaction,
        error: TransactionError
    )
    func transactionManager(
        _ transactionManager: TransactionManagerProtocol,
        didFailToRemoveTransaction transaction: Transaction,
        error: TransactionError
    )
}

extension TransactionManagerDelegate {
    func transactionManager(
        _ transactionManager: TransactionManagerProtocol,
        didRetrieveTransactions transactions: [Transaction]
    ) {}
    func transactionManager(
        _ transactionManager: TransactionManagerProtocol,
        didAddTransaction transaction: Transaction,
        in transactions: [Transaction]
    ) {}
    func transactionManager(
        _ transactionManager: TransactionManagerProtocol,
        didRemoveTransaction transaction: Transaction,
        from transactions: [Transaction]
    ) {}
    func transactionManager(
        _ transactionManager: TransactionManagerProtocol,
        didFailToRetrieveTransactions error: TransactionError
    ) {}
    func transactionManager(
        _ transactionManager: TransactionManagerProtocol,
        didFailToAppendTransaction transaction: Transaction,
        error: TransactionError
    ) {}
    func transactionManager(
        _ transactionManager: TransactionManagerProtocol,
        didFailToRemoveTransaction transaction: Transaction,
        error: TransactionError
    ) {}
}

// MARK: - TransactionManagerProtocol

protocol TransactionManagerProtocol: AnyObject {
    func appendTransaction(_ transaction: Transaction)
    func removeTransaction(_ transaction: Transaction)
    func retrieveTransactions()

    func addObserver(_ observer: TransactionManagerDelegate)
    func removeObserver(_ observer: TransactionManagerDelegate)
}
