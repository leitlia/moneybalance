//
//  AppLifeCycleEventProvider.swift
//  MoneyBalance
//
//  Created by Arnold Leitli on 2022. 03. 05..
//

import Foundation

// MARK: - AppLifeCycleEventProviderDelegate

protocol AppLifeCycleEventProviderDelegate: AnyObject {
    func appLifeCycleEventProviderSceneWillEnterForeground(
        _ appLifeCycleEventProvider: AppLifeCycleEventProviderProtocol
    )
    func appLifeCycleEventProviderSceneDidEnterBackground(
        _ appLifeCycleEventProvider: AppLifeCycleEventProviderProtocol
    )
}

// MARK: - AppLifeCycleEventProviderProtocol

protocol AppLifeCycleEventProviderProtocol {
    func sceneWillEnterForeground()
    func sceneDidEnterBackground()
    func addObserver(_ observer: AppLifeCycleEventProviderDelegate)
    func removeObserver(_ observer: AppLifeCycleEventProviderDelegate)
}

// MARK: - AppLifeCycleEventProvider

class AppLifeCycleEventProvider {
    private let observers: Observable

    init(observers: Observable) {
        self.observers = observers
    }
}

// MARK: AppLifeCycleEventProviderProtocol

extension AppLifeCycleEventProvider: AppLifeCycleEventProviderProtocol {
    func sceneWillEnterForeground() {
        notifyObserversSceneWillEnterForeground()
    }

    func sceneDidEnterBackground() {
        notifyObserversSceneDidEnterBackground()
    }

    func addObserver(_ observer: AppLifeCycleEventProviderDelegate) {
        observers.addObserver(observer)
    }

    func removeObserver(_ observer: AppLifeCycleEventProviderDelegate) {
        observers.removeObserver(observer)
    }
}

extension AppLifeCycleEventProvider {
    private func notifyObserversSceneWillEnterForeground() {
        notify { delegate in
            delegate.appLifeCycleEventProviderSceneWillEnterForeground(self)
        }
    }

    private func notifyObserversSceneDidEnterBackground() {
        notify { delegate in
            delegate.appLifeCycleEventProviderSceneDidEnterBackground(self)
        }
    }

    private func notify(method: (AppLifeCycleEventProviderDelegate) -> Void) {
        for subscriber in observers.subscribers {
            if let subscriber = subscriber as? AppLifeCycleEventProviderDelegate {
                method(subscriber)
            }
        }
    }
}
