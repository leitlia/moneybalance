//
//  CoreDataManager.swift
//  MoneyBalance
//
//  Created by Arnold Leitli on 2022. 03. 05..
//

import CoreData
import Foundation

// MARK: - CoreDataManagerProtocol

protocol CoreDataManagerProtocol: AnyObject {
    var delegate: CoreDataManagerDelegate? { get set }
    func retrieveData(for object: NSManagedObject.Type)
}

// MARK: - CoreDataManagerTransactionProtocol

protocol CoreDataManagerTransactionProtocol: CoreDataManagerProtocol {
    func save(transaction: Transaction)
    func remove(transaction: Transaction)
}

// MARK: - CoreDataManagerDelegate

protocol CoreDataManagerDelegate: AnyObject {
    func coreDataManager(
        _ coreDataManager: CoreDataManagerProtocol,
        didRetrieveData data: [Any]
    )
    func coreDataManager(
        _ coreDataManager: CoreDataManagerProtocol,
        didSaveData data: Any
    )
    func coreDataManager(
        _ coreDataManager: CoreDataManagerProtocol,
        didFailSaveData data: Any
    )
    func coreDataManager(
        _ coreDataManager: CoreDataManagerProtocol,
        didRemoveData data: Any
    )
    func coreDataManager(
        _ coreDataManager: CoreDataManagerProtocol,
        didFailRemoveData data: Any
    )
}

// MARK: - CoreDataManager

class CoreDataManager {
    weak var delegate: CoreDataManagerDelegate?

    private let context: NSManagedObjectContextProtocol
    private let applicationLifeCycleEventProvider: AppLifeCycleEventProviderProtocol

    init(
        context: NSManagedObjectContextProtocol,
        applicationLifeCycleEventProvider: AppLifeCycleEventProviderProtocol
    ) {
        self.context = context
        self.applicationLifeCycleEventProvider = applicationLifeCycleEventProvider
        applicationLifeCycleEventProvider.addObserver(self)
    }
}

// MARK: CoreDataManagerProtocol

extension CoreDataManager: CoreDataManagerProtocol {
    func retrieveData(for object: NSManagedObject.Type) {
        do {
            let items = try context.fetch(object.fetchRequest())
            delegate?.coreDataManager(self, didRetrieveData: items)
        } catch {
            print(error)
        }
    }
}

// MARK: CoreDataManagerTransactionProtocol

extension CoreDataManager: CoreDataManagerTransactionProtocol {
    func save(transaction: Transaction) {
        let newItem = TransactionCoreObject(
            context: context as! NSManagedObjectContext
        )
        newItem.amount = Int64(transaction.amount.integerValue)
        newItem.transactionDescription = transaction.transactionDescription
        newItem.createdAt = transaction.createdAt
        newItem.id = transaction.uuid
        newItem.transactionType = Int16(transaction.type.rawValue)
        
        do {
            try context.save()
            try context.obtainPermanentIDs(for: [newItem])
            var newTransaction = transaction
            newTransaction.persistReference = newItem.objectID
            delegate?.coreDataManager(self, didSaveData: newTransaction)
        } catch {
            delegate?.coreDataManager(self, didFailSaveData: transaction)
        }
    }

    func remove(transaction: Transaction) {
        guard let objectID = transaction.persistReference as? NSManagedObjectID else {
            delegate?.coreDataManager(self, didRemoveData: transaction)
            return
        }

        do {
            let object = try context.existingObject(with: objectID)
            context.delete(object)
            try context.save()
            delegate?.coreDataManager(self, didRemoveData: transaction)
        } catch {
            delegate?.coreDataManager(self, didFailRemoveData: transaction)
        }
    }
}

// MARK: AppLifeCycleEventProviderDelegate

extension CoreDataManager: AppLifeCycleEventProviderDelegate {
    func appLifeCycleEventProviderSceneWillEnterForeground(
        _ appLifeCycleEventProvider: AppLifeCycleEventProviderProtocol
    ) {}

    func appLifeCycleEventProviderSceneDidEnterBackground(
        _ appLifeCycleEventProvider: AppLifeCycleEventProviderProtocol
    ) {
        saveContext()
    }
}

extension CoreDataManager {
    private func saveContext() {
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                let nserror = error as NSError
                delegate?.coreDataManager(self, didFailSaveData: nserror)
            }
        }
    }
}
