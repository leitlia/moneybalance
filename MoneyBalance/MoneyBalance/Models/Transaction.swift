//
//  Transaction.swift
//  MoneyBalance
//
//  Created by Arnold Leitli on 2022. 03. 01..
//

import Foundation

// MARK: - TransactionType

enum TransactionType: Int {
    case income
    case expense
}

// MARK: - Transaction

struct Transaction: Equatable {
    static func == (lhs: Transaction, rhs: Transaction) -> Bool {
        lhs.uuid == rhs.uuid
    }
    
    var uuid: String = UUID().uuidString
    var persistReference: AnyObject?
    var amount = Amount()
    var type = TransactionType.income
    var transactionDescription = ""
    var createdAt = Date()
}
