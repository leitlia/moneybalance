//
//  Amount.swift
//  MoneyBalance
//
//  Created by Arnold Leitli on 2022. 03. 06..
//

import Foundation

class Amount: Equatable, Comparable {
    private(set) var integerValue = 0

    var isZero: Bool { integerValue == 0 }

    private let currencyFormatter: CurrencyStringFormatter
    
    init(
        _ value: Int = 0,
        currencyFormatter: CurrencyStringFormatter = CurrencyStringFormatter()
    ) {
        integerValue = value
        self.currencyFormatter = currencyFormatter
    }

    

    func append(value: String) {
        let amountString = String(integerValue)
        let finalString = amountString + value
        if finalString.count > 14 { return }
        let result = (Int(finalString) ?? 0)
        integerValue = result
    }

    func remove() {
        let amountString = String(integerValue)
        let finalString = amountString.dropLast()
        integerValue = (Int(finalString) ?? 0)
    }
    
    var currencyString: String {
        currencyFormatter.currencyString(from: integerValue)
    }

    func clear() {
        integerValue = 0
    }
    
    static func < (lhs: Amount, rhs: Amount) -> Bool {
        lhs.integerValue < rhs.integerValue
    }
    
    static func == (lhs: Amount, rhs: Amount) -> Bool {
        lhs.integerValue == rhs.integerValue
    }
}

