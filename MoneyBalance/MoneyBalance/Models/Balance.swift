//
//  Balance.swift
//  MoneyBalance
//
//  Created by Arnold Leitli on 2022. 03. 01..
//

import Foundation

struct Balance {
    var income = 0
    var expense = 0

    var balance: Int {
        income - expense
    }
}
