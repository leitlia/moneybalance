//
//  OrderedTransactionList.swift
//  MoneyBalance
//
//  Created by Arnold Leitli on 2022. 03. 01..
//

import Foundation

struct OrderedTransactionList {
    private(set) var balance = Balance()
    private(set) var orderedTransactions: [[Transaction]]
    private(set) var allItemCount: Int
    /*
     timecomplexity
     N transactions
     M transaction dates by day

     O(N) going through transactions picking the right groups
     O(M*logM) sorting the dates

     */

    init(transactions: [Transaction]) {
        var groupedTransactions = [String: [Transaction]]()
        var dates = [String]()
        var expenses = 0
        var incomes = 0
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .long
        dateFormatter.timeStyle = .none

        for transaction in transactions {
            let dateString = dateFormatter.string(from: transaction.createdAt)

            if groupedTransactions[dateString] == nil {
                dates.append(dateString)
            }

            if transaction.type == .expense {
                expenses += transaction.amount.integerValue
            } else {
                incomes += transaction.amount.integerValue
            }

            groupedTransactions[dateString, default: []] += [transaction]
        }

        let orderedDates = dates.sorted { leftString, rightString in
            let date1 = dateFormatter.date(from: leftString) ?? Date()
            let date2 = dateFormatter.date(from: rightString) ?? Date()
            return date1 > date2
        }

        var result = [[Transaction]]()

        for date in orderedDates {
            let groupOfTransactions = groupedTransactions[date, default: []]
            let sortedTransactions = groupOfTransactions.sorted(by: { t1, t2 in
                t1.createdAt < t2.createdAt
            })
            result.append(sortedTransactions)
        }

        allItemCount = transactions.count
        balance = Balance(income: incomes, expense: expenses)
        orderedTransactions = result
    }
}
