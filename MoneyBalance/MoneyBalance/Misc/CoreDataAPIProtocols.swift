//
//  CoreDataAPIProtocols.swift
//  MoneyBalance
//
//  Created by Arnold Leitli on 2022. 03. 06..
//

import Foundation
import CoreData

protocol NSManagedObjectContextProtocol {
    var hasChanges: Bool { get }
    
    func fetch<T>(_ request: NSFetchRequest<T>) throws -> [T] where T : NSFetchRequestResult
    func save() throws
    func obtainPermanentIDs(for objects: [NSManagedObject]) throws
    func existingObject(with objectID: NSManagedObjectID) throws -> NSManagedObject
    func delete(_ object: NSManagedObject)
}

extension NSManagedObjectContext: NSManagedObjectContextProtocol {
    
}
