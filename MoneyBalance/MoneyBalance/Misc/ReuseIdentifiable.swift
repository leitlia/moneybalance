//
//  ReuseIdentifiable.swift
//  MoneyBalance
//
//  Created by Arnold Leitli on 2022. 03. 06..
//

import Foundation

// MARK: - ReuseIdentifiable

protocol ReuseIdentifiable {
    static var reuseIdentifier: String { get }
}

extension ReuseIdentifiable {
    static var reuseIdentifier: String {
        String(describing: self)
    }
}
