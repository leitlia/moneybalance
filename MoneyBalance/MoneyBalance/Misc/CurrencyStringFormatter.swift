//
//  CurrencyStringFormatter.swift
//  MoneyBalance
//
//  Created by Arnold Leitli on 2022. 03. 05..
//

import Foundation

class CurrencyStringFormatter: NumberFormatter {
    init(
        locale: Locale = Locale(identifier: "en_US"),
        currencyCode: String = "USD"
    ) {
        super.init()
        self.locale = locale
        self.currencyCode = currencyCode
        numberStyle = .currency
        maximumFractionDigits = 2
        usesGroupingSeparator = false
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func currencyString(from amount: Int) -> String {
        string(from: Double(amount) / 100 as NSNumber) ?? "No valid"
    }
}
