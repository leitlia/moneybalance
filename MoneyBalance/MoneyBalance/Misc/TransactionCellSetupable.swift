//
//  TransactionCellSetupable.swift
//  MoneyBalance
//
//  Created by Arnold Leitli on 2022. 03. 06..
//

import Foundation

// MARK: - TransactionCellSetupable

protocol TransactionCellSetupable {
    func setup(with transaction: Transaction)
}
