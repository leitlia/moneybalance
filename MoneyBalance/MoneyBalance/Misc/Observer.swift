//
//  Observable.swift
//  MoneyBalance
//
//  Created by Arnold Leitli on 2022. 03. 01..
//

import Foundation

// MARK: - Observable

protocol Observable {
    var subscribers: [AnyObject] { get }
    func addObserver(_ object: AnyObject)
    func removeObserver(_ object: AnyObject)
}

// MARK: - Observer

final class Observer: Observable {
    private let dispatchQueue: DispatchQueue

    private(set) var subscribers: [AnyObject]
    
    init(dispatchQueue: DispatchQueue) {
        subscribers = []
        self.dispatchQueue = dispatchQueue
    }

    func addObserver(_ object: AnyObject) {
        dispatchQueue.async(flags: .barrier) { [weak self] in
            guard let strongSelf = self else { return }
            if strongSelf.subscribers.firstIndex(where: { (item) -> Bool in
                item === object
            }) == nil {
                strongSelf.subscribers.append(object)
            }
        }
    }

    func removeObserver(_ object: AnyObject) {
        dispatchQueue.sync(flags: .barrier) { [weak self] in
            guard let strongSelf = self else { return }
            if let index = strongSelf.subscribers
                .firstIndex(where: { (item) -> Bool in
                    item === object
                }) {
                strongSelf.subscribers.remove(at: index)
            }
        }
    }
}
